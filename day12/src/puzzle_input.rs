use nom::{
    self,
    bytes::complete::tag,
    character::complete::{self, line_ending, multispace0, newline, not_line_ending, one_of},
    combinator::{map, peek},
    error::Error,
    multi::{many1, separated_list1},
    sequence::{delimited, separated_pair, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub raw_lines: Vec<&'a str>,
    pub spring_rows: Vec<SpringRow>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct SpringRow {
    pub springs: Vec<char>,
    pub broken_groups: Vec<usize>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let spring_list = map(
        separated_pair(
            many1(one_of(".?#")),
            tag(" "),
            separated_list1(tag(","), map(complete::u32, |n| n as usize)),
        ),
        |(springs, contiguous_groups)| SpringRow {
            springs,
            broken_groups: contiguous_groups,
        },
    );

    let puzzle_input = separated_list1(newline, spring_list);

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, spring_lists)| PuzzleInput {
                raw_lines,
                spring_rows: spring_lists,
            },
        ),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
