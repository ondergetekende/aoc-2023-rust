use rayon::iter::{IntoParallelIterator, ParallelIterator};

use crate::puzzle_input::*;
use crate::solver::Solver;

pub fn solve(input: &PuzzleInput) -> String {
    input
        .clone()
        .spring_rows
        .into_par_iter()
        .map(|spring_list| {
            let q: String = spring_list.springs.iter().collect();
            let s = Solver::new(q.as_str(), spring_list.broken_groups.as_slice());

            s.solve()
        })
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {

    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "21");
    }

    #[test]
    #[ignore]
    fn test_actual_solve() {
        let input = PuzzleInput::try_from(INPUT).unwrap();
        assert_eq!(solve(&input), "8419");
    }
}
