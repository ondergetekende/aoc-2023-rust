use rayon::iter::{IntoParallelIterator, ParallelIterator};

use crate::puzzle_input::*;
use crate::solver::Solver;

pub fn solve(input: &PuzzleInput) -> String {
    let input = input.clone();
    input
        .spring_rows
        .into_par_iter()
        .map(|spring_list| {
            let springs_str: String = spring_list.springs.iter().collect();

            let dup_springs = (0..5)
                .map(|_| springs_str.clone())
                .collect::<Vec<String>>()
                .join("?");
            let dup_broken_list = (0..5)
                .flat_map(|_| spring_list.broken_groups.iter().cloned())
                .collect::<Vec<_>>();

            let solver = Solver::new(&dup_springs, dup_broken_list.as_slice());
            solver.solve()
        })
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "525152");
    }
    #[test]
    fn test_actual_solve() {
        let input = PuzzleInput::try_from(INPUT).unwrap();
        assert_eq!(solve(&input), "160500973317706");
    }
}
