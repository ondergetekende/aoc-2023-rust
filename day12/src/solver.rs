use factorial::Factorial;
use std::{cell::RefCell, cmp::min, collections::HashMap};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Solver<'a> {
    springs: &'a str,
    broken_groups: &'a [usize],
    cache: RefCell<HashMap<(&'a str, &'a [usize]), usize>>,
}

impl<'a> Solver<'a> {
    pub fn new(springs: &'a str, broken_groups: &'a [usize]) -> Self {
        Self {
            springs,
            broken_groups,
            cache: RefCell::new(HashMap::new()),
        }
    }

    pub fn solve(&'a self) -> usize {
        self.count_possibilities(self.springs, self.broken_groups)
    }

    /// Solve the trivial cases, where the answer doesn't require any iteration.
    fn solve_trivial(&self, springs: &'a str, broken_groups: &[usize]) -> Option<usize> {
        // We can ignore any known working springs at the edges, as they don't affect the
        // outcome.
        let springs = springs.trim_matches(|c| c == '.');

        let (n_broken, n_unknown) =
            springs
                .chars()
                .fold((0, 0), |(n_broken, n_unknown), c| match c {
                    '.' => (n_broken, n_unknown),
                    '#' => (n_broken + 1, n_unknown),
                    _ => (n_broken, n_unknown + 1),
                });
        let num_broken_springs = broken_groups.iter().sum::<usize>();

        if n_broken == 0 && n_unknown == 0 {
            // No broken springs.
            return Some(if broken_groups.is_empty() { 1 } else { 0 });
        }

        if broken_groups.is_empty() {
            // No broken springs.
            return Some(if n_broken == 0 { 1 } else { 0 });
        }

        if n_broken + n_unknown < num_broken_springs {
            // There are insufficient places to locate the remaining broken springs
            return Some(0);
        }

        if springs.len() == num_broken_springs {
            // The entire row needs to be broken
            return Some(if broken_groups.len() == 1 { 1 } else { 0 });
        }

        if n_broken > num_broken_springs {
            // There are more known broken springs than the spring list allows for
            return Some(0);
        }

        if num_broken_springs + broken_groups.len() - 1 > springs.len() {
            // Including the working springs between the groups of broken springs,
            // there are more springs than the row is long.
            return Some(0);
        }

        if n_unknown < 20 && n_unknown == springs.len() {
            // With all-unknown rows, we can just count the possibilities instead of iterating over them
            // This uses a factorial. The max factorial that fits in a usize is 20!.

            let n_unknown_working = n_unknown - num_broken_springs;
            // Find the number of working springs that don't have a defacto location (e.g. after a broken spring)
            let n_to_distribute = n_unknown_working + 1 - broken_groups.len();
            // Find where to place the working springs
            let n_good_groups = broken_groups.len();

            // from https://math.stackexchange.com/questions/1462099
            return Some(
                (n_to_distribute + n_good_groups).factorial()
                    / n_to_distribute.factorial()
                    / n_good_groups.factorial(),
            );
        }

        self.cache.borrow().get(&(springs, broken_groups)).cloned()
    }

    fn count_possibilities(&'a self, springs: &'a str, broken_groups: &'a [usize]) -> usize {
        if let Some(result) = self.solve_trivial(springs, broken_groups) {
            return result;
        }

        let springs = springs.trim_matches(|c| c == '.');

        // println!("Non-trivial: {} {:?}", springs, broken_groups);
        let roughly_third = springs.len() / 3;

        // Find a point to split the row, so we can divide and conquer.
        let split_point = springs
            .chars()
            .skip(roughly_third)
            .take(roughly_third)
            .position(|c| c == '.')
            .map(|idx| idx + roughly_third);

        let result = match split_point {
            None => self.brute_force_possibilities(springs, broken_groups),
            Some(idx) => {
                let (left_springs, right_springs) = springs.split_at(idx);

                (0..=broken_groups.len())
                    .map(|idx_group| {
                        let (left_groups, right_groups) = broken_groups.split_at(idx_group);

                        let possible_left = self.solve_trivial(left_springs, left_groups);
                        let possible_right = self.solve_trivial(right_springs, right_groups);

                        if let Some(0) = possible_left {
                            // This specific split is impossible.
                            return 0;
                        }

                        if let Some(0) = possible_right {
                            // This specific split is impossible.
                            return 0;
                        }

                        let left_count = possible_left
                            .unwrap_or_else(|| self.count_possibilities(left_springs, left_groups));
                        let right_count = possible_right.unwrap_or_else(|| {
                            self.count_possibilities(right_springs, right_groups)
                        });

                        left_count * right_count
                    })
                    .sum::<usize>()
            }
        };

        self.cache
            .borrow_mut()
            .insert((springs, broken_groups), result);

        result
    }

    fn brute_force_possibilities(&'a self, springs: &'a str, broken_groups: &'a [usize]) -> usize {
        let springs = springs.trim_matches(|c| c == '.');

        // Determine the maximum number of working springs at the start of the row.
        let length_required = broken_groups.iter().sum::<usize>() + broken_groups.len() - 1;
        if length_required > springs.len() {
            // There are more broken springs than the row is long.
            return 0;
        }

        let group = broken_groups[0];
        let min_offset = springs
            .chars()
            .take(group)
            .enumerate()
            .find_map(|(idx, c)| if c == '.' { Some(idx) } else { None })
            .unwrap_or(0);

        let result = (min_offset..=(springs.len() - length_required))
            .map(|broken_start| {
                if springs.chars().nth(broken_start + group) == Some('#') {
                    // There's a broken spring after the list broken springs.
                    // This means the list should've been longer, had it started at the indicated_position
                    return 0;
                }

                if springs[broken_start..broken_start + group]
                    .chars()
                    .any(|c| c == '.')
                {
                    // There's a working spring in the broken group.
                    return 0;
                }

                if springs[0..broken_start].chars().any(|c| c == '#') {
                    // There's a broken spring in the working group.
                    return 0;
                }

                let k = min(broken_start + group + 1, springs.len());

                let result = self.count_possibilities(
                    springs[k..].trim_start_matches(|c| c == '.'),
                    &broken_groups[1..],
                );
                result
            })
            .sum();

        result
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[rstest]
    #[case("?", &[1], 1)]
    #[case("??.", &[1], 2)]
    #[case("???.###", &[3], 1)]
    #[case("???", &[3], 1)]
    #[case("???.", &[3], 1)]
    #[case("???", &[1,1], 1)]
    #[case( "..#.45.6",  &[1,1,1], 2)]
    #[case("???.??.?",  &[1,1,1], 9)]
    #[case( ".##?.#", &[2,1], 1)]
    #[case( "????", &[2,1], 1)]
    #[case( "?????", &[2,1], 3)]
    #[case( "????", &[1,1], 3)] // #.#. #..# .#.#
    #[case( "????????", &[1,1], 21)]
    #[case("??.??###.?", &[1, 5], 2)]
    #[case("###.?", &[5], 0)]
    // // From puzzle examples:
    #[case("???.###", &[1, 1, 3], 1)]
    #[case( ".??..??...?##.", &[1, 1, 3], 4)]
    #[case( "?#?#?#?#?#?#?#?", &[1, 3, 1, 6], 1)]
    #[case( "????.#...#...", &[4, 1, 1], 1)]
    #[case( "????.######..#####.",  &[1, 6, 5], 4)]
    #[case( "?###????????", &[3, 2, 1], 10)]
    fn test_row_simple_solve(
        #[case] springs: &str,
        #[case] broken_groups: &[usize],
        #[case] expected: usize,
    ) {
        let solver = Solver::new(springs, broken_groups);
        assert_eq!(solver.solve(), expected);
    }
}
