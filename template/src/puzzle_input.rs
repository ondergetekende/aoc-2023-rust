use nom::{
    self,
    bytes::complete::tag,
    character::complete::{line_ending, multispace0, not_line_ending},
    combinator::{map, peek},
    error::Error,
    multi::separated_list1,
    sequence::{delimited, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'puzzle> {
    pub raw_lines: Vec<&'puzzle str>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let puzzle_input = tag("todo");

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, _)| PuzzleInput { raw_lines },
        ),
        multispace0,
    );

    parser(input)
}

impl<'puzzle> TryFrom<&'puzzle str> for PuzzleInput<'puzzle> {
    type Error = Error<&'puzzle str>;

    fn try_from(s: &'puzzle str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[rstest]
    #[case::test_input(TEST_INPUT)]
    #[case::final_input(INPUT)]
    /// Verifies that the test input is valid.
    fn test_puzzle_input_from_test_input(#[case] input: &str) {
        if !input.is_empty() {
            let input = PuzzleInput::try_from(input).unwrap();
            assert!(!input.raw_lines.is_empty());
        }
    }

    #[test]
    /// Verifes that invalid input is rejected.
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        input.unwrap_err();
    }
}
