use crate::puzzle_input::PuzzleInput;

pub fn solve(_input: &PuzzleInput) -> String {
    todo!()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "");
    }
}
