use crate::puzzle_input::*;

pub fn solve(input: &PuzzleInput) -> String {
    input
        .times
        .iter()
        .zip(input.distances.iter())
        .map(|(time, distance)| get_wincount(*time as i64, *distance as i64))
        .product::<i64>()
        .to_string()
}
pub fn get_wincount(time: i64, distance: i64) -> i64 {
    // distance_over_finish = speed * (time - speed) - distance
    // distance_over_finish = - speed^2 + speed * time - distance
    // applying abc formula:
    // speed = (time +- sqrt(time^2 - 4 * distance)) / 2

    let d = time * time - 4 * distance;
    let mut vals = [
        ((time as f64 - (d as f64).sqrt()) / 2.0).ceil() as i64,
        ((time as f64 + (d as f64).sqrt()) / 2.0).floor() as i64,
    ];

    // Special case: we need to beat the record, so we need to exceed the distance.
    // We need to remove results that meet the record
    if vals[0] * (time - vals[0]) == distance {
        vals[0] += 1;
        print!("removing headwin");
    }
    if vals[1] * (time - vals[1]) == distance {
        vals[1] -= 1;
        print!("removing tailwin");
    }

    vals[1] - vals[0] + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_wincount() {
        assert_eq!(get_wincount(7, 9), 4);
        assert_eq!(get_wincount(15, 40), 8);
        assert_eq!(get_wincount(30, 200), 9);
    }

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "288");
    }
}
