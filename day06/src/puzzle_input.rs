use nom::{
    self,
    bytes::complete::{is_not, tag},
    character::complete::{self, line_ending, multispace0, multispace1},
    combinator::{map, peek},
    error::Error,
    multi::separated_list1,
    sequence::{preceded, separated_pair, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub raw_lines: Vec<&'a str>,

    pub times: Vec<i32>,
    pub distances: Vec<i32>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let input = input.trim();
    let times = preceded(
        tuple((tag("Time:"), multispace0)),
        separated_list1(multispace1, complete::i32),
    );
    let distances = preceded(
        tuple((tag("Distance:"), multispace0)),
        separated_list1(multispace1, complete::i32),
    );

    let puzzle_parser = separated_pair(times, line_ending, distances);
    let multiline_parser = separated_list1(line_ending, is_not("\n"));

    let mut parser = map(
        tuple((peek(puzzle_parser), multiline_parser)),
        |((times, distances), raw_lines)| PuzzleInput {
            raw_lines,
            times,
            distances,
        },
    );
    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();

        assert_eq!(input.times, vec![7, 15, 30]);
        assert_eq!(input.distances, vec![9, 40, 200]);
        assert_eq!(
            input.raw_lines,
            vec!["Time:      7  15   30", "Distance:  9  40  200"]
        );
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
