use crate::puzzle_input::*;
use crate::puzzle_part1::get_wincount;

pub fn solve(input: &PuzzleInput) -> String {
    let time = input.raw_lines[0]
        .chars()
        .filter(|c| c.is_ascii_digit())
        .collect::<String>()
        .parse::<i64>()
        .unwrap();
    let distance = input.raw_lines[1]
        .chars()
        .filter(|c| c.is_ascii_digit())
        .collect::<String>()
        .parse::<i64>()
        .unwrap();

    get_wincount(time, distance).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "71503");
    }
}
