use nom::{
    self,
    character::complete::{alphanumeric1, multispace0, newline},
    error::Error,
    multi::separated_list1,
    sequence::delimited,
    Finish, IResult, Parser,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub lines: Vec<&'a str>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let mut parser = delimited(
        multispace0,
        separated_list1(newline, alphanumeric1).map(|lines| PuzzleInput { lines }),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_input() {
        let input = PuzzleInput::try_from(INPUT);

        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_test_input() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();

        assert_eq!(input.lines.len(), 4);
        assert_eq!(input.lines[0], "1abc2");
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
