use crate::puzzle_input::*;

pub fn solve_part_1(input: &PuzzleInput) -> String {
    let nums = input.lines.iter().map(|line| {
        let numbers = line
            .chars()
            .filter(|c| c.is_ascii_digit())
            .map(|c| c.to_digit(10).unwrap() as i32)
            .collect::<Vec<i32>>();

        numbers.first().unwrap() * 10 + numbers.iter().last().unwrap()
    });

    nums.sum::<i32>().to_string()
}

pub fn solve_part_2(_input: &PuzzleInput) -> String {
    // We can't use nom to parse this (i think), as nom would parse "eightwo" as 8.
    // It should be 2 for the last digit, and 8 for the first digit.
    let posible_digits = vec![
        ("1", 1),
        ("2", 2),
        ("3", 3),
        ("4", 4),
        ("5", 5),
        ("6", 6),
        ("7", 7),
        ("8", 8),
        ("9", 9),
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ];

    _input
        .lines
        .iter()
        .map(|line| {
            let first_digit = (0..line.len())
                .filter_map(|pos| {
                    let linepart = &line[pos..];

                    posible_digits.iter().find_map(|(word, digit)| {
                        if linepart.starts_with(word) {
                            Some(*digit)
                        } else {
                            None
                        }
                    })
                })
                .next()
                .unwrap();
            let last_digit = (0..line.len())
                .rev()
                .filter_map(|pos| {
                    let linepart = &line[pos..];

                    posible_digits.iter().find_map(|(word, digit)| {
                        if linepart.starts_with(word) {
                            Some(*digit)
                        } else {
                            None
                        }
                    })
                })
                .next()
                .unwrap();

            first_digit * 10 + last_digit
        })
        .sum::<i32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const TEST_INPUT2: &str = include_str!("../test_input2.txt");

    #[test]
    fn test_solve_part_1() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve_part_1(&input), "142");
    }

    #[test]
    fn test_solve_part_2() {
        let input = PuzzleInput::try_from(TEST_INPUT2).unwrap();
        assert_eq!(solve_part_2(&input), "281");
    }
}
