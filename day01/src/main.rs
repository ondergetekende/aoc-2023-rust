mod puzzle;
mod puzzle_input;

fn main() {
    let puzzle_input = include_str!("../input.txt");

    // Solve the puzzle for each input
    match puzzle_input::PuzzleInput::try_from(puzzle_input) {
        Err(e) => {
            println!("Error: {:?}", e);
        }
        Ok(input) => {
            println!("Part 1: {}", puzzle::solve_part_1(&input));
            println!("Part 2: {}", puzzle::solve_part_2(&input));
        }
    };
}
