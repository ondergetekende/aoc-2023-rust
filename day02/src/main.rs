mod puzzle;
mod puzzle_input;

fn main() {
    let puzzle_input = include_str!("../input.txt");

    // Solve the puzzle for each input
    match puzzle_input::PuzzleInput::try_from(puzzle_input) {
        Err(e) => {
            println!("Error: {:?}", e);
        }
        Ok(input) => {
            println!("day02 - part 1: {}", puzzle::solve_part_1(&input));
            println!("day02 - part 2: {}", puzzle::solve_part_2(&input));
        }
    };
}
