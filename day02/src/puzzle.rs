use crate::puzzle_input::*;

pub fn solve_part_1(input: &PuzzleInput) -> String {
    let thresholds = Set {
        red: 12,
        green: 13,
        blue: 14,
    };

    input
        .rounds
        .iter()
        .filter_map(|round| {
            match round.sets.iter().all(|set| {
                set.red <= thresholds.red
                    && set.green <= thresholds.green
                    && set.blue <= thresholds.blue
            }) {
                true => Some(round.index),
                false => None,
            }
        })
        .sum::<u32>()
        .to_string()
}

pub fn solve_part_2(input: &PuzzleInput) -> String {
    input
        .rounds
        .iter()
        .map(|round| {
            let max_red = round.sets.iter().map(|set| set.red).max().unwrap();
            let max_green = round.sets.iter().map(|set| set.green).max().unwrap();
            let max_blue = round.sets.iter().map(|set| set.blue).max().unwrap();
            max_red * max_green * max_blue
        })
        .sum::<u32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve_part_1() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve_part_1(&input), "8");
    }

    #[test]
    fn test_solve_part_2() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve_part_2(&input), "2286");
    }
}
