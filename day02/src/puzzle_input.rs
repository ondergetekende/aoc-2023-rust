use std::ops::Add;

use nom::{
    self,
    branch::alt,
    bytes::complete::tag,
    character::complete::{multispace0, newline},
    character::complete::{space0, u32 as nom_u32},
    combinator::map,
    error::Error,
    multi::separated_list1,
    sequence::{delimited, tuple},
    Finish, IResult, Parser,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput {
    pub rounds: Vec<Round>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Round {
    pub index: u32,
    pub sets: Vec<Set>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Set {
    pub red: u32,
    pub green: u32,
    pub blue: u32,
}

impl Add for Set {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Set {
            red: self.red + rhs.red,
            green: self.green + rhs.green,
            blue: self.blue + rhs.blue,
        }
    }
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let cube = map(
        tuple((
            nom_u32::<_, Error<&str>>,
            space0,
            alt((tag("red"), tag("green"), tag("blue"))),
        )),
        |(value, _, color)| match color {
            "red" => Set {
                red: value,
                green: 0,
                blue: 0,
            },
            "green" => Set {
                red: 0,
                green: value,
                blue: 0,
            },
            "blue" => Set {
                red: 0,
                green: 0,
                blue: value,
            },
            _ => unreachable!(),
        },
    );

    let set = map(separated_list1(tag(", "), cube), |sets| {
        sets.iter().fold(
            Set {
                red: 0,
                green: 0,
                blue: 0,
            },
            |acc, set| acc + set.clone(),
        )
    });

    let round = tuple((
        delimited(tag("Game "), nom_u32, tag(": ")),
        separated_list1(tag("; "), set),
    ))
    .map(|(index, sets)| Round { index, sets });

    let mut parser = delimited(
        multispace0,
        separated_list1(newline, round).map(|rounds| PuzzleInput { rounds }),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT);
        let input = input.unwrap();
        assert_eq!(input.rounds[0].sets[0].red, 4);
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
