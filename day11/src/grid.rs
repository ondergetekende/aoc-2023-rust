use std::{
    fmt,
    ops::{Index, IndexMut},
};

use crate::vector2d::{Direction, Vector2D};

#[derive(PartialEq, Clone)]
pub struct Grid<T> {
    stride: usize,
    cells: Vec<T>,
}

impl<T> Grid<T> {
    pub fn height(&self) -> i32 {
        (self.cells.len() / self.stride) as i32
    }

    pub fn width(&self) -> i32 {
        self.stride as i32
    }

    fn offset<K: GridKey>(&self, pos: &K) -> usize {
        let pos: (i32, i32) = (*pos).to_grid_key();
        (pos.1 * self.stride as i32 + pos.0) as usize
    }

    pub fn get<K: GridKey>(&self, pos: &K) -> Option<&T> {
        if self.valid_position(pos) {
            Some(&self.cells[self.offset(pos)])
        } else {
            None
        }
    }
    pub fn get_mut<K: GridKey>(&mut self, pos: &K) -> Option<&mut T> {
        if self.valid_position(pos) {
            let offset = self.offset(pos);
            Some(&mut self.cells[offset])
        } else {
            None
        }
    }

    // pub fn valid_position<U: GridKey>(&self, pos: U) -> bool {
    //     let pos = pos.into();
    //     pos.0 >= 0
    //         && pos.1 >= 0
    //         && pos.0 < self.stride as i32
    //         && pos.1 < self.cells.len() as i32 / self.stride as i32
    // }

    pub fn valid_position<U: GridKey>(&self, pos: &U) -> bool {
        let pos: (i32, i32) = (*pos).to_grid_key();
        pos.0 >= 0
            && pos.1 >= 0
            && pos.0 < self.stride as i32
            && pos.1 < self.cells.len() as i32 / self.stride as i32
    }

    pub fn map<U>(&self, f: impl Fn(&T) -> U) -> Grid<U> {
        let cells = self.cells.iter().map(f).collect();
        Grid {
            stride: self.stride,
            cells,
        }
    }

    pub fn entry<U: GridKey>(&self, pos: &U) -> GridEntry<T> {
        GridEntry {
            grid: self,
            origin: pos.to_grid_key(),
        }
    }

    /// Returns an iterator over all valid positions in the grid.
    pub fn iter(&self) -> impl Iterator<Item = GridEntry<T>> {
        let height = self.cells.len() / self.stride;
        let width = self.stride;
        (0..height).flat_map(move |y| {
            (0..width).map(move |x| GridEntry {
                grid: self,
                origin: (x as i32, y as i32),
            })
        })
    }

    pub fn rows(&self) -> impl Iterator<Item = impl Iterator<Item = GridEntry<T>>> {
        (0..self.height()).map(move |y| {
            (0..self.width()).map(move |x| GridEntry {
                grid: self,
                origin: (x, y),
            })
        })
    }

    pub fn columns(&self) -> impl Iterator<Item = impl Iterator<Item = GridEntry<T>>> {
        (0..self.width()).map(move |x| {
            (0..self.height()).map(move |y| GridEntry {
                grid: self,
                origin: (x, y),
            })
        })
    }
}

impl<T> From<Vec<Vec<T>>> for Grid<T>
where
    T: fmt::Debug,
{
    fn from(v: Vec<Vec<T>>) -> Self {
        let stride = v[0].len();

        let mut cells = Vec::<T>::with_capacity(v.len() * stride);
        cells.extend(
            v.into_iter()
                .inspect(|a| assert!(a.len() == stride, "Rows must be of equal length"))
                .flatten(),
        );

        Self { stride, cells }
    }
}

impl<T, U> Index<&U> for Grid<T>
where
    U: GridKey,
{
    type Output = T;

    fn index(&self, position: &U) -> &Self::Output {
        self.get(position).expect("Index outside grid")
    }
}

impl<T, U> IndexMut<&U> for Grid<T>
where
    U: GridKey,
{
    fn index_mut(&mut self, position: &U) -> &mut Self::Output {
        self.get_mut(position).expect("Index outside grid")
    }
}

/// ----------------------------------------------------------------------------------------

pub trait GridKey {
    fn to_grid_key(&self) -> (i32, i32);
}

impl GridKey for (i32, i32) {
    fn to_grid_key(&self) -> (i32, i32) {
        *self
    }
}

impl GridKey for Vector2D {
    fn to_grid_key(&self) -> (i32, i32) {
        (*self).into()
    }
}

/// ----------------------------------------------------------------------------------------

pub struct GridEntry<'a, T> {
    grid: &'a Grid<T>,
    pub origin: (i32, i32),
}

impl<'a, T> GridEntry<'a, T> {
    // Returns whether view originates at a valid position in the grid.
    pub fn is_valid(&self) -> bool {
        self.grid.valid_position(&self.origin)
    }

    pub fn around8(&self) -> impl Iterator<Item = GridEntry<'a, T>> + '_ {
        Direction::CARDINAL_8
            .iter()
            .map(move |dir| self.relative(&Vector2D::from(*dir)))
    }
    pub fn around4(&self) -> impl Iterator<Item = GridEntry<'a, T>> + '_ {
        Direction::CARDINAL_4
            .iter()
            .map(move |dir| self.relative(&Vector2D::from(*dir)))
    }

    pub fn relative<U: GridKey>(&self, pos: &U) -> GridEntry<'a, T> {
        let pos = pos.to_grid_key();
        GridEntry {
            grid: self.grid,
            origin: (self.origin.0 + pos.0, self.origin.1 + pos.1),
        }
    }

    pub fn get(&self) -> Option<&T> {
        self.grid.get(&self.origin)
    }
}

impl<T> fmt::Display for Grid<T>
where
    char: From<T>,
    T: Copy,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f)?;
        for y in 0..self.height() {
            for x in 0..self.width() {
                let cell = &self.cells[self.offset(&(x, y))];
                let c: char = char::from(*cell);
                write!(f, "{}", c)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl<T> fmt::Debug for Grid<T>
where
    char: From<T>,
    T: Copy,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        (self as &dyn fmt::Display).fmt(f)
    }
}
