use std::{
    cmp::{max, min},
    collections::BTreeSet,
};

use itertools::Itertools;

use crate::puzzle_input::*;
fn total_distance(input: &PuzzleInput, factor: i64) -> i64 {
    let star_locations = input.grid.iter().filter_map(|cell| match cell.get() {
        Some('#') => Some(cell.origin),
        _ => None,
    });

    let empty_rows = input
        .grid
        .rows()
        .enumerate()
        .filter_map(|(y, mut row)| {
            if row.all(|cell| *cell.get().unwrap() == '.') {
                Some(y as i32)
            } else {
                None
            }
        })
        .collect::<BTreeSet<_>>();

    let empty_cols = input
        .grid
        .columns()
        .enumerate()
        .filter_map(|(x, mut column)| {
            if column.all(|cell| *cell.get().unwrap() == '.') {
                Some(x as i32)
            } else {
                None
            }
        })
        .collect::<BTreeSet<_>>();

    star_locations
        .permutations(2)
        .map(|stars| {
            let p0 = (min(stars[0].0, stars[1].0), min(stars[0].1, stars[1].1));
            let p1 = (max(stars[0].0, stars[1].0), max(stars[0].1, stars[1].1));

            let x_range = p0.0..=p1.0;
            let y_range = p0.1..=p1.1;

            let empty_cols_included =
                empty_cols.iter().filter(|x| x_range.contains(x)).count() as i32;
            let empty_rows_included =
                empty_rows.iter().filter(|y| y_range.contains(y)).count() as i32;

            let base_distance = ((p1.0 - p0.0) + (p1.1 - p0.1)) as i64;
            let crossed_empty = (empty_cols_included + empty_rows_included) as i64;

            base_distance + crossed_empty * (factor - 1)
        })
        .sum::<i64>()
        / 2
}
// All combinations are counted twice, so divide by 2
pub fn solve(input: &PuzzleInput) -> String {
    let result = total_distance(input, 1000000);
    (result).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "82000210");
    }
    #[test]
    fn test_solve2() {
        let input = PuzzleInput::try_from(
            "
...#......
.......x..
x.........
..........
......x...
.x........
.........x
..........
.......#..
x...x.....
",
        )
        .unwrap();
        assert_eq!(total_distance(&input, 2), 15);
    }
}
