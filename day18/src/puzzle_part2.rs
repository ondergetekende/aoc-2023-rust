use aoc_grid::{Coordinate, Direction};

use crate::puzzle_input::PuzzleInput;

fn wrapping_windows<T>(elements: &[T]) -> impl Iterator<Item = (&'_ T, &'_ T)> {
    elements
        .windows(2)
        .map(|w| (&w[0], &w[1]))
        .chain(std::iter::once((
            elements.last().unwrap(),
            elements.first().unwrap(),
        )))
}

// Calculates the area of the enclosed region of the given coordinates.
// Uses the shoelace theorem
fn calculate_area(coords: &[Coordinate]) -> usize {
    let area = wrapping_windows(coords)
        .map(|(a, b)| (b.0 + a.0) * (b.1 - a.1))
        .sum::<isize>()
        .unsigned_abs()
        / 2;

    // The shoe-lace theorem computes a half-open polygon, meaning
    // half of the circumference is not included in the area.
    let circumference = wrapping_windows(coords)
        .map(|(a, b)| (a.0 - b.0).abs() + (a.1 - b.1).abs())
        .sum::<isize>()
        .unsigned_abs();

    area + circumference / 2 + 1
}

pub fn solve(input: &PuzzleInput) -> String {
    let mut pos = Coordinate(0, 0);
    let mut coordinates = vec![];
    for line in input.raw_lines.iter() {
        let (_, instruction) = line.split_once("(#").unwrap();
        let distance = isize::from_str_radix(&instruction[0..5], 16).unwrap();
        let direction = match instruction.chars().nth(5).unwrap() {
            '0' => Direction::Right,
            '1' => Direction::Down,
            '2' => Direction::Left,
            '3' => Direction::Up,
            _d => panic!("Unknown direction: {_d}"),
        };
        pos += direction * distance;
        coordinates.push(pos);
    }

    calculate_area(&coordinates).to_string()
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_calculate_area_3x3() {
        let area = calculate_area(&[
            Coordinate(1, 1),
            Coordinate(3, 1),
            Coordinate(3, 3),
            Coordinate(1, 3),
        ]);
        assert_eq!(area, 9)
    }
    #[test]
    fn test_calculate_area_3x4() {
        let area = calculate_area(&[
            Coordinate(1, 1),
            Coordinate(4, 1),
            Coordinate(4, 3),
            Coordinate(1, 3),
        ]);
        assert_eq!(area, 12)
    }

    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(include_str!("../test_input.txt")).unwrap();
        assert_eq!(solve(&input), "952408144115");
    }
}
