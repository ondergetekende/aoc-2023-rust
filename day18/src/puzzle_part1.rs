use aoc_grid::{Coordinate, Direction, Grid};

use crate::puzzle_input::PuzzleInput;

pub fn calculate_area(grid: &Grid<bool>) -> usize {
    let mut target_grid = Grid::<bool>::new(grid.width() as usize, grid.height() as usize);
    let mut area = grid.width() * grid.height();
    let mut to_fill = Vec::<Coordinate>::new();

    let (w, h) = (grid.width(), grid.height());

    to_fill.extend((0..w).map(|x| Coordinate(x, 0)).filter(|c| !grid[c]));
    to_fill.extend((0..w).map(|x| Coordinate(x, h - 1)).filter(|c| !grid[c]));
    to_fill.extend((0..h).map(|y| Coordinate(0, y)).filter(|c| !grid[c]));
    to_fill.extend((0..h).map(|y| Coordinate(w - 1, y)).filter(|c| !grid[c]));

    while let Some(pos) = to_fill.pop() {
        // println!("{:?}", pos);
        if target_grid[pos] {
            continue; // already filled
        }

        if grid[pos] {
            continue; // Ran into a wall
        }

        target_grid[pos] = true;
        area -= 1;

        for dir in Direction::CARDINAL_4.iter() {
            let new_pos = pos + dir;
            if grid.coordinate_valid(new_pos) {
                to_fill.push(new_pos);
            }
        }
    }

    area as usize
}

pub fn solve(input: &PuzzleInput) -> String {
    // Determine the max size of the grid we need
    let mut pos_max = Coordinate::new(0, 0);
    let mut pos_min = Coordinate::new(0, 0);
    let mut pos = Coordinate::new(0, 0);
    input.dig_lines.iter().for_each(|dig_line| {
        pos += dig_line.direction * dig_line.distance as isize;
        pos_max.0 = pos_max.0.max(pos.0);
        pos_max.1 = pos_max.1.max(pos.1);
        pos_min.0 = pos_min.0.min(pos.0);
        pos_min.1 = pos_min.1.min(pos.1);
    });

    // Plot the trench on the grid
    let mut grid = Grid::<bool>::new(
        (pos_max.0 - pos_min.0) as usize + 1,
        (pos_max.1 - pos_min.1) as usize + 1,
    );
    let mut pos = -pos_min;

    input.dig_lines.iter().for_each(|dig_line| {
        for _ in 0..dig_line.distance {
            grid[pos] = true;
            pos += dig_line.direction;
        }
        pos_max.0 = pos_max.0.max(pos.0);
        pos_max.1 = pos_max.1.max(pos.1);
    });

    // calculate the area of the grid
    calculate_area(&grid).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_negative() {
        let input = PuzzleInput::try_from(
            "
L 3 (#ffffff)
U 3 (#ffffff)
R 3 (#ffffff)
D 3 (#ffffff)
        ",
        )
        .unwrap();
        assert_eq!(solve(&input), "16");
    }

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "62");
    }

    #[test]
    fn test_actual_solve() {
        let input = PuzzleInput::try_from(include_str!("../input.txt")).unwrap();
        assert_eq!(solve(&input).parse::<usize>().unwrap(), 67891);
    }
}
