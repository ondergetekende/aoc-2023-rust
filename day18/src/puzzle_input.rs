use aoc_grid::Direction;

use nom::{
    self,
    bytes::complete::tag,
    character::complete::{
        self, hex_digit1, line_ending, multispace0, multispace1, not_line_ending, one_of,
    },
    combinator::{map, peek},
    error::Error,
    multi::separated_list1,
    sequence::{delimited, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct DigLine {
    pub direction: Direction,
    pub distance: usize,
    pub color: [u8; 3],
}

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'puzzle> {
    pub raw_lines: Vec<&'puzzle str>,
    pub dig_lines: Vec<DigLine>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let dig_line = map(
        tuple((
            map(one_of("URLD"), |c| match c {
                'U' => Direction::Up,
                'R' => Direction::Right,
                'L' => Direction::Left,
                'D' => Direction::Down,
                _ => unreachable!(),
            }),
            multispace1,
            complete::u64,
            multispace1,
            delimited(tag("(#"), hex_digit1, tag(")")),
        )),
        |(direction, _, length, _, color)| DigLine {
            direction,
            distance: length as usize,
            color: hex::decode(color).unwrap().try_into().unwrap(),
        },
    );

    let puzzle_input = separated_list1(line_ending, dig_line);

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, dig_lines)| PuzzleInput {
                raw_lines,
                dig_lines,
            },
        ),
        multispace0,
    );

    parser(input)
}

impl<'puzzle> TryFrom<&'puzzle str> for PuzzleInput<'puzzle> {
    type Error = Error<&'puzzle str>;

    fn try_from(s: &'puzzle str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[rstest]
    #[case::test_input(TEST_INPUT)]
    #[case::final_input(INPUT)]
    /// Verifies that the test input is valid.
    fn test_puzzle_input_from_test_input(#[case] input: &str) {
        if !input.is_empty() {
            let input = PuzzleInput::try_from(input).unwrap();
            assert!(!input.raw_lines.is_empty());
            assert_eq!(input.raw_lines.len(), input.dig_lines.len());
        }
    }

    #[test]
    /// Verifes that invalid input is rejected.
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        input.unwrap_err();
    }
}
