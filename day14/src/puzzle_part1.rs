use aoc_grid::GridRange;

use crate::puzzle_input::*;

pub fn column_load(column: impl Iterator<Item = Terrain> + ExactSizeIterator) -> usize {
    // Iterator starts at North, where equal to column height
    let len = column.len();
    let mut resting_point_weight = len;
    let mut result = 0;

    column.enumerate().for_each(|(i, terrain)| match terrain {
        Terrain::Round => {
            result += resting_point_weight;
            resting_point_weight -= 1;
        }
        Terrain::Cube => resting_point_weight = len - i - 1,
        Terrain::Empty => (),
    });
    result
}

pub fn solve(input: &PuzzleInput) -> String {
    input
        .terrain
        .columns()
        .map(|col| column_load(col.iter().copied()))
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case("##", 0)]
    #[case("OO", 3)]
    #[case("O.", 2)]
    #[case(".O", 2)]
    #[case("#O.", 2)]
    #[case("#.O", 2)]
    fn test_find_load(#[case] terrain_def: &str, #[case] expected: usize) {
        let terrain = terrain_def.chars().map(Terrain::from).collect::<Vec<_>>();
        assert_eq!(column_load(terrain.iter().copied()), expected);
    }

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "136");
    }
}
