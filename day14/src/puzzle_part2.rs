use std::collections::HashMap;

use aoc_grid::{Coordinate, Direction, Grid};

use crate::puzzle_input::*;

pub fn move_line(grid: &mut Grid<Terrain>, start_point: Coordinate, direction: Direction) {
    // Movethe last block one away from the start point. That will act as a virtual square rock
    // on the side of the grid, against which the round rocks will roll.
    let mut last_block = start_point - direction;

    let mut current_point = start_point;
    let mut sphere_count = 0;
    let mut empty_count = 0;

    loop {
        // Find what's on the current point, but if we reach the edge, assume there's a cube there.
        let val = grid.get(current_point).unwrap_or(&Terrain::Cube);
        match val {
            // For non-cube blocks, keep track of what we found
            Terrain::Round => sphere_count += 1,
            Terrain::Empty => empty_count += 1,

            // Once we encounter a cube, rewrite all the blocks.
            // e.g. .o.oo... -> ooo.....
            Terrain::Cube => {
                last_block += direction;
                for _ in 0..empty_count {
                    grid[last_block] = Terrain::Empty;
                    last_block += direction;
                }
                for _ in 0..sphere_count {
                    grid[last_block] = Terrain::Round;
                    last_block += direction;
                }
                debug_assert!(last_block == current_point);
                empty_count = 0;
                sphere_count = 0;
                last_block = current_point;
            }
        }

        if !grid.coordinate_valid(current_point) {
            break;
        }

        // print!("{current_point:?}",);
        current_point += direction;
    }

    // println!("After moving line: {}", grid);
}

pub fn move_stones(grid: &mut Grid<Terrain>, direction: Direction) {
    let mut start_point = match direction {
        Direction::Up => (0, grid.height() - 1),
        Direction::Right => (0, 0),
        Direction::Down => (grid.width() - 1, 0),
        Direction::Left => (grid.width() - 1, grid.height() - 1),
        _ => unreachable!(),
    };

    let lateral_direction = direction.clockwise_4();

    while grid.coordinate_valid(start_point) {
        move_line(grid, start_point.into(), direction);
        start_point += lateral_direction;
    }
}

pub fn spin_cycle(mut grid: Grid<Terrain>) -> Grid<Terrain> {
    move_stones(&mut grid, Direction::Up);
    move_stones(&mut grid, Direction::Left);
    move_stones(&mut grid, Direction::Down);
    move_stones(&mut grid, Direction::Right);
    grid
}

pub fn find_north_stress(grid: &Grid<Terrain>) -> isize {
    grid.iter_entries()
        .filter(|t| **t == Terrain::Round)
        .map(|t| grid.height() - t.coordinate.1)
        .sum::<isize>()
}

pub fn solve(input: &PuzzleInput) -> String {
    let mut grid = input.terrain.clone();
    let mut grid_iteration = HashMap::<Grid<Terrain>, usize>::new();
    let mut iteration_stress = Vec::<isize>::with_capacity(1000000000);
    for iteration in 0..1000000000 {
        if let Some(prev_iteration) = grid_iteration.get(&grid) {
            let cycle_length = iteration - prev_iteration;
            let cycle_start = prev_iteration;
            let cycle_index = (1000000000 - cycle_start - 1) % cycle_length + cycle_start;

            return iteration_stress[cycle_index].to_string();
        }

        grid_iteration.insert(grid.clone(), iteration);
        grid = spin_cycle(grid);
        iteration_stress.push(find_north_stress(&grid));
    }

    find_north_stress(&grid).to_string()
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[rstest]
    #[case("##", "##")]
    #[case("#...#", "#...#")]
    #[case("#O..#", "#..O#")]
    #[case("#O.O#", "#.OO#")]
    #[case("#OOO#", "#OOO#")]
    fn test_move_line(#[case] grid: &str, #[case] expected: &str) {
        let mut grid: Grid<Terrain> =
            Grid::from(vec![grid.chars().collect()]).map(|c| Terrain::from(*c));

        let start_point = (0, 0).into();
        move_line(&mut grid, start_point, Direction::Right);

        assert_eq!(
            grid.iter().map(|t| char::from(*t)).collect::<String>(),
            expected
        );
    }

    #[rstest]
    #[case("##", "##")]
    #[case("OO", "OO")]
    #[case(".O", ".O")]
    #[case("O.", ".O")]
    #[case("#O.", "#..")]
    #[case("#.O", "#..")]
    #[case(".O#", ".O#")]
    #[case("O.#", ".O#")]
    #[ignore]
    fn test_move_rocks(#[case] terrain_def: &str, #[case] expected: &str) {
        use aoc_grid::GridRange;

        let mut terrain = PuzzleInput::try_from(terrain_def).unwrap().terrain;
        move_stones(&mut terrain, Direction::Right);

        assert_eq!(
            terrain
                .row(0)
                .iter()
                .map(|t| char::from(*t))
                .collect::<String>(),
            expected
        );
    }

    #[test]
    fn test_solve_part1() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        let mut grid = input.terrain.clone();
        move_stones(&mut grid, Direction::Up);

        println!("After moving up{}", &grid);
        assert_eq!(find_north_stress(&grid), 136);
    }
    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "64");
    }
}
