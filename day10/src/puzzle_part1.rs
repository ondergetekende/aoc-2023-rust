use crate::puzzle_input::*;
use crate::vector2d::*;

pub fn find_loop(input: &PuzzleInput) -> Vec<Vector2D> {
    let grid = &input.grid;

    // Simulate comint into the start from a valid direction
    let mut last_direction = if grid[&input.start].north {
        Direction::Down
    } else if grid[&input.start].east {
        Direction::Left
    } else if grid[&input.start].south {
        Direction::Up
    } else if grid[&input.start].west {
        Direction::Right
    } else {
        panic!("No exit found");
    };

    let mut positions = vec![input.start];

    // Find a loop
    loop {
        let pos = positions.last().unwrap();
        let part = grid[pos];
        if part.north && last_direction != Direction::Down {
            last_direction = Direction::Up;
        } else if part.east && last_direction != Direction::Left {
            last_direction = Direction::Right;
        } else if part.south && last_direction != Direction::Up {
            last_direction = Direction::Down;
        } else if part.west && last_direction != Direction::Right {
            last_direction = Direction::Left;
        } else {
            panic!("No exit found");
        }

        // If we've reached the start again, we've found the loop, and we're done
        let new_pos: Vector2D = *pos + Vector2D::from(last_direction);
        if new_pos == input.start {
            break;
        }

        debug_assert!(!positions.contains(&new_pos));
        positions.push(new_pos);
    }

    positions
}

pub fn solve(input: &PuzzleInput) -> String {
    let the_loop = find_loop(input);
    // In theory, there could be a second loop, but not of the example or the input rely on it.

    (the_loop.len() / 2).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const TEST_INPUT2: &str = include_str!("../test_input2.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "4");
    }
    #[test]
    fn test_solve2() {
        let input = PuzzleInput::try_from(TEST_INPUT2).unwrap();
        assert_eq!(solve(&input), "8");
    }
}
