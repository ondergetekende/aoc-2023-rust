use nom::{
    self,
    character::complete::{line_ending, multispace0, newline, not_line_ending, one_of},
    combinator::{map, peek},
    error::Error,
    multi::{many1, separated_list1},
    sequence::{delimited, tuple},
    Finish, IResult,
};

use crate::{
    grid::Grid,
    vector2d::{Direction, Vector2D},
};

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct MapPart {
    pub north: bool,
    pub east: bool,
    pub south: bool,
    pub west: bool,
}

impl MapPart {
    pub const START: MapPart = MapPart {
        north: true,
        east: true,
        south: true,
        west: true,
    };

    pub const EMPTY: MapPart = MapPart {
        north: false,
        east: false,
        south: false,
        west: false,
    };

    pub fn is_start(&self) -> bool {
        self.north && self.east && self.south && self.west
    }
}

impl From<char> for MapPart {
    fn from(c: char) -> Self {
        match c {
            '.' => MapPart {
                north: false,
                east: false,
                south: false,
                west: false,
            },
            'S' => MapPart {
                north: true,
                east: true,
                south: true,
                west: true,
            },

            'L' => MapPart {
                north: true,
                east: true,
                south: false,
                west: false,
            },
            'J' => MapPart {
                north: true,
                east: false,
                south: false,
                west: true,
            },
            '7' => MapPart {
                north: false,
                east: false,
                south: true,
                west: true,
            },
            'F' => MapPart {
                north: false,
                east: true,
                south: true,
                west: false,
            },
            '|' => MapPart {
                north: true,
                east: false,
                south: true,
                west: false,
            },
            '-' => MapPart {
                north: false,
                east: true,
                south: false,
                west: true,
            },

            '└' => MapPart {
                north: true,
                east: true,
                south: false,
                west: false,
            },
            '┘' => MapPart {
                north: true,
                east: false,
                south: false,
                west: true,
            },
            '┐' => MapPart {
                north: false,
                east: false,
                south: true,
                west: true,
            },
            '┌' => MapPart {
                north: false,
                east: true,
                south: true,
                west: false,
            },
            '│' => MapPart {
                north: true,
                east: false,
                south: true,
                west: false,
            },
            '─' => MapPart {
                north: false,
                east: true,
                south: false,
                west: true,
            },

            _ => unreachable!(),
        }
    }
}

impl From<&MapPart> for char {
    fn from(m: &MapPart) -> char {
        match m {
            MapPart {
                north: false,
                east: false,
                south: false,
                west: false,
            } => '.',
            MapPart {
                north: true,
                east: true,
                south: true,
                west: true,
            } => '*',

            MapPart {
                north: true,
                east: true,
                south: false,
                west: false,
            } => '└',
            MapPart {
                north: true,
                east: false,
                south: false,
                west: true,
            } => '┘',
            MapPart {
                north: false,
                east: false,
                south: true,
                west: true,
            } => '┐',
            MapPart {
                north: false,
                east: true,
                south: true,
                west: false,
            } => '┌',
            MapPart {
                north: true,
                east: false,
                south: true,
                west: false,
            } => '│',
            MapPart {
                north: false,
                east: true,
                south: false,
                west: true,
            } => '─',

            _ => unreachable!(),
        }
    }
}
impl From<MapPart> for char {
    fn from(m: MapPart) -> char {
        match m {
            MapPart {
                north: false,
                east: false,
                south: false,
                west: false,
            } => '.',
            MapPart {
                north: true,
                east: true,
                south: true,
                west: true,
            } => '*',

            MapPart {
                north: true,
                east: true,
                south: false,
                west: false,
            } => '└',
            MapPart {
                north: true,
                east: false,
                south: false,
                west: true,
            } => '┘',
            MapPart {
                north: false,
                east: false,
                south: true,
                west: true,
            } => '┐',
            MapPart {
                north: false,
                east: true,
                south: true,
                west: false,
            } => '┌',
            MapPart {
                north: true,
                east: false,
                south: true,
                west: false,
            } => '│',
            MapPart {
                north: false,
                east: true,
                south: false,
                west: true,
            } => '─',

            _ => unreachable!(),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub raw_lines: Vec<&'a str>,
    pub grid: Grid<MapPart>,
    pub start: Vector2D,
}

impl<'a> PuzzleInput<'a> {
    fn new(mut grid: Grid<MapPart>, raw_lines: Vec<&'a str>) -> Self {
        let start_field = grid
            .iter()
            .find_map(|p| {
                if MapPart::START == *p.get().unwrap() {
                    Some(p)
                } else {
                    None
                }
            })
            .unwrap();

        // The starting position does not indicate a direction, so we'll need to derive that
        // By looking at the adjacent cells, and seeing where the connect
        let adjacent = [
            start_field
                .relative(&Vector2D::from(Direction::Up))
                .get()
                .unwrap_or(&MapPart::EMPTY)
                .south,
            start_field
                .relative(&Vector2D::from(Direction::Right))
                .get()
                .unwrap_or(&MapPart::EMPTY)
                .west,
            start_field
                .relative(&Vector2D::from(Direction::Down))
                .get()
                .unwrap_or(&MapPart::EMPTY)
                .north,
            start_field
                .relative(&Vector2D::from(Direction::Left))
                .get()
                .unwrap_or(&MapPart::EMPTY)
                .east,
        ];
        let start_pos = start_field.origin;

        grid[&start_pos] = MapPart {
            north: adjacent[0],
            east: adjacent[1],
            south: adjacent[2],
            west: adjacent[3],
        };

        Self {
            raw_lines,
            grid,
            start: start_pos.into(),
        }
    }
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let puzzle_input = separated_list1(newline, many1(map(one_of("LJ7F|-S."), MapPart::from)));

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, puzzle)| PuzzleInput::new(Grid::from(puzzle), raw_lines),
        ),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const TEST_INPUT2: &str = include_str!("../test_input2.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_test_input2() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT2);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
