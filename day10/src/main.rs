pub mod grid;
pub mod puzzle_input;
pub mod puzzle_part1;
pub mod puzzle_part2;
pub mod vector2d;

#[cfg(feature = "dhat-heap")]
#[global_allocator]
static ALLOC: dhat::Alloc = dhat::Alloc;

fn main() {
    #[cfg(feature = "dhat-heap")]
    let _profiler = dhat::Profiler::new_heap();

    let puzzle_input = include_str!("../input.txt");

    // Solve the puzzle for each input
    match puzzle_input::PuzzleInput::try_from(puzzle_input) {
        Err(e) => {
            println!("Error: {:?}", e);
        }
        Ok(input) => {
            println!("day10 - part 1: {}", puzzle_part1::solve(&input));
            println!("day10 - part 2: {}", puzzle_part2::solve(&input));
        }
    };
}
