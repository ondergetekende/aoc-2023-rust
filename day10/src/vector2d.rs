use std::ops::{Add, AddAssign, Mul, MulAssign, Neg, Sub, SubAssign};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Vector2D {
    pub x: i32,
    pub y: i32,
}

const ADJACENT_POSITIONS: [Vector2D; 8] = [
    Vector2D { x: -1, y: -1 },
    Vector2D { x: 0, y: -1 },
    Vector2D { x: 1, y: -1 },
    Vector2D { x: -1, y: 0 },
    Vector2D { x: 1, y: 0 },
    Vector2D { x: -1, y: 1 },
    Vector2D { x: 0, y: 1 },
    Vector2D { x: 1, y: 1 },
];

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,

    UpRight,
    UpLeft,
    DownRight,
    DownLeft,
}

impl Direction {
    pub const CARDINAL_4: [Self; 4] = [Self::Up, Self::Right, Self::Down, Self::Left];
    pub const CARDINAL_8: [Self; 8] = [
        Self::Up,
        Self::UpRight,
        Self::Right,
        Self::DownRight,
        Self::Down,
        Self::DownLeft,
        Self::Left,
        Self::UpLeft,
    ];
}

impl From<Direction> for Vector2D {
    fn from(dir: Direction) -> Self {
        match dir {
            Direction::Up => Self::new(0, -1),
            Direction::Down => Self::new(0, 1),
            Direction::Left => Self::new(-1, 0),
            Direction::Right => Self::new(1, 0),

            Direction::UpRight => Self::new(1, -1),
            Direction::UpLeft => Self::new(-1, -1),
            Direction::DownRight => Self::new(1, 1),
            Direction::DownLeft => Self::new(-1, 1),
        }
    }
}

impl Vector2D {
    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    pub fn adjacent_positions(self) -> impl Iterator<Item = Vector2D> {
        ADJACENT_POSITIONS
            .iter()
            .map(move |pos| *pos + self)
            .filter(move |pos| pos.x >= 0 && pos.y >= 0)
    }
}

impl From<(i32, i32)> for Vector2D {
    fn from((x, y): (i32, i32)) -> Self {
        Self { x, y }
    }
}

impl From<Vector2D> for (i32, i32) {
    fn from(val: Vector2D) -> Self {
        (val.x, val.y)
    }
}

impl<T> AddAssign<T> for Vector2D
where
    T: Into<Vector2D>,
{
    fn add_assign(&mut self, rhs: T) {
        let rhs: Vector2D = rhs.into();
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl<T> Add<T> for Vector2D
where
    T: Into<Vector2D>,
{
    type Output = Self;

    fn add(self, rhs: T) -> Self::Output {
        let mut result = self;
        result += rhs;
        result
    }
}

impl<T> SubAssign<T> for Vector2D
where
    T: Into<Vector2D>,
{
    fn sub_assign(&mut self, rhs: T) {
        let rhs: Vector2D = rhs.into();
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl<T> Sub<T> for Vector2D
where
    T: Into<Vector2D>,
{
    type Output = Self;

    fn sub(self, rhs: T) -> Self::Output {
        let mut result = self;
        result -= rhs;
        result
    }
}

impl Neg for Vector2D {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self::new(-self.x, -self.y)
    }
}

impl Mul<u32> for Vector2D {
    type Output = Self;

    fn mul(self, rhs: u32) -> Self::Output {
        Self::new(self.x * rhs as i32, self.y * rhs as i32)
    }
}

impl Mul<i32> for Vector2D {
    type Output = Self;

    fn mul(self, rhs: i32) -> Self::Output {
        Self::new(self.x * rhs, self.y * rhs)
    }
}

impl MulAssign<u32> for Vector2D {
    fn mul_assign(&mut self, rhs: u32) {
        *self = *self * rhs;
    }
}
