use std::collections::HashSet;

use crate::puzzle_input::*;
use crate::puzzle_part1::*;

pub fn solve(input: &PuzzleInput) -> String {
    let our_loop = find_loop(input)
        .into_iter()
        .map(|v| v.into())
        .collect::<HashSet<(i32, i32)>>();

    input
        .grid
        .rows()
        .map(|row| {
            let mut interior_count = 0;
            let mut inside_loop_n = false;
            let mut inside_loop_s = false;

            row.for_each(|g| {
                let map_part = g.get().unwrap();
                if our_loop.contains(&g.origin) {
                    if map_part.north {
                        inside_loop_n = !inside_loop_n;
                    }
                    if map_part.south {
                        inside_loop_s = !inside_loop_s;
                    }
                } else if inside_loop_n && inside_loop_s {
                    interior_count += 1;
                }
            });
            interior_count
        })
        .sum::<i32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const TEST_INPUT2: &str = include_str!("../test_input2.txt");
    const TEST_INPUT3: &str = include_str!("../test_input3.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "1");
    }
    #[test]
    fn test_solve2() {
        let input = PuzzleInput::try_from(TEST_INPUT2).unwrap();
        assert_eq!(solve(&input), "1");
    }
    #[test]
    fn test_solve3() {
        let input = PuzzleInput::try_from(TEST_INPUT3).unwrap();
        assert_eq!(solve(&input), "10");
    }
}
