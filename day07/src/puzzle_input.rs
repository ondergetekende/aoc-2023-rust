use nom::{
    self,
    bytes::complete::is_a,
    character::complete::{self, line_ending, multispace0, newline, not_line_ending, space1},
    combinator::{map, peek},
    error::Error,
    multi::separated_list1,
    sequence::{delimited, separated_pair, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub raw_lines: Vec<&'a str>,
    pub rounds: Vec<Round<'a>>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Round<'a> {
    pub cards: &'a str,
    pub bet: u32,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let round = map(
        separated_pair(is_a("KQJT98765432A"), space1, complete::u32),
        |(cards, bet): (&str, u32)| Round { cards, bet },
    );

    let puzzle_input = separated_list1(newline, round);

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, rounds)| PuzzleInput { raw_lines, rounds },
        ),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert!(!input.raw_lines.is_empty());

        // check that all rounds are unique. The puzzle question doesn't mention that,
        // but it would affect the output if they weren't.
        assert!(input.rounds.iter().all(|round| input
            .rounds
            .iter()
            .filter(|r| r.cards == round.cards)
            .count()
            == 1));
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT).unwrap();
        assert!(!input.raw_lines.is_empty());

        // check that all rounds are unique. The puzzle question doesn't mention that,
        // but it would affect the output if they weren't.
        assert!(input.rounds.iter().all(|round| input
            .rounds
            .iter()
            .filter(|r| r.cards == round.cards)
            .count()
            == 1));
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
