use std::{cmp::Ordering, collections::BTreeMap};

use crate::puzzle_input::*;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum PokerHandType {
    FiveOfAKind = 6,
    FourOfAKind = 5,
    FullHouse = 4,
    ThreeOfAKind = 3,
    TwoPair = 2,
    OnePair = 1,
    HighCard = 0,
}

const CARD_ORDER: &str = "J23456789TQKA";

impl PartialOrd for PokerHandType {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for PokerHandType {
    fn cmp(&self, other: &Self) -> Ordering {
        (*self as usize).cmp(&(*other as usize))
    }
}

fn score_hand(hand: &str) -> PokerHandType {
    if hand == "JJJJJ" {
        // Handle JJJJJ here, as we'd end up with an empty hand
        // after removing the jacks
        return PokerHandType::FiveOfAKind;
    }

    let mut card_count = hand
        .chars()
        .fold(BTreeMap::new(), |mut acc: BTreeMap<char, usize>, c| {
            *acc.entry(c).or_insert(0) += 1;
            acc
        });

    // let jack_count = card_count.get(&'J').unwrap_or(&0);
    let jack_count = card_count.remove_entry(&'J').unwrap_or(('J', 0)).1;

    let mut cards = card_count.into_values().collect::<Vec<_>>();
    cards.sort();
    cards.reverse();
    cards[0] += jack_count;

    if cards[0] == 5 {
        PokerHandType::FiveOfAKind
    } else if cards[0] == 4 {
        PokerHandType::FourOfAKind
    } else if cards[0] == 3 && cards[1] == 2 {
        PokerHandType::FullHouse
    } else if cards[0] == 3 {
        PokerHandType::ThreeOfAKind
    } else if cards[0] == 2 && cards[1] == 2 {
        PokerHandType::TwoPair
    } else if cards[0] == 2 {
        PokerHandType::OnePair
    } else {
        PokerHandType::HighCard
    }
}

fn compare_hand<'a>(hand1: &'a str, hand2: &'a str) -> Ordering {
    let score1 = score_hand(hand1);
    let score2 = score_hand(hand2);

    if score1 != score2 {
        return score1.cmp(&score2);
    }

    for (c1, c2) in hand1.chars().zip(hand2.chars()) {
        let c1_idx = CARD_ORDER.find(c1).unwrap();
        let c2_idx = CARD_ORDER.find(c2).unwrap();

        if c1_idx != c2_idx {
            return c1_idx.cmp(&c2_idx);
        }
    }

    Ordering::Equal
}

pub fn solve(input: &PuzzleInput) -> String {
    let mut rounds = input.rounds.iter().collect::<Vec<_>>();

    rounds.sort_by(|a, b| compare_hand(a.cards, b.cards));

    rounds
        .iter()
        .enumerate()
        .map(|(idx, round)| (idx + 1) * round.bet as usize)
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_scores() {
        // From the examples:
        assert_eq!(score_hand("32T3K"), PokerHandType::OnePair);
        assert_eq!(score_hand("KTJJT"), PokerHandType::FourOfAKind);
        assert_eq!(score_hand("KK677"), PokerHandType::TwoPair);
        assert_eq!(score_hand("T55J5"), PokerHandType::FourOfAKind);
        assert_eq!(score_hand("QQQJA"), PokerHandType::FourOfAKind);

        // Some others:
        assert_eq!(score_hand("KKKKK"), PokerHandType::FiveOfAKind);
        assert_eq!(score_hand("KKKKQ"), PokerHandType::FourOfAKind);
        // assert_eq!(score_hand("KKKQQ"), PokerHandType::FullHouse);
        // assert_eq!(score_hand("KKKJA"), PokerHandType::ThreeOfAKind);
        // assert_eq!(score_hand("KKJJA"), PokerHandType::TwoPair);
        // assert_eq!(score_hand("KKJQA"), PokerHandType::OnePair);
        // assert_eq!(score_hand("KQJTA"), PokerHandType::HighCard);
    }

    #[test]
    fn test_compares() {
        // These are the examples from the problem description
        let examples = ["32T3K", "KK677", "T55J5", "QQQJA", "KTJJT"];

        for i in 0..examples.len() - 1 {
            println!("Comparing {} and {}", examples[i], examples[i + 1]);
            assert_eq!(compare_hand(examples[i], examples[i + 1]), Ordering::Less);
            // assert_eq!(
            //     compare_hand(examples[i + 1], examples[i]),
            //     Ordering::Greater
            // );
            // assert_eq!(compare_hand(examples[i], examples[i]), Ordering::Equal,);
        }
    }

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "5905");
    }
}
