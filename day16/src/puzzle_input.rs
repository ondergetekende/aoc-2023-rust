use aoc_grid::Grid;
use nom::{
    self,
    bytes::complete::is_a,
    character::complete::{line_ending, multispace0, newline, not_line_ending},
    combinator::{map, peek},
    error::Error,
    multi::separated_list1,
    sequence::{delimited, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum MapPiece {
    Empty,
    SplitterH,
    SplitterV,
    MirrorTopLeft,
    MirrorTopRight,
}

impl From<char> for MapPiece {
    fn from(value: char) -> Self {
        match value {
            '.' => MapPiece::Empty,
            '-' => MapPiece::SplitterH,
            '|' => MapPiece::SplitterV,
            '\\' => MapPiece::MirrorTopLeft,
            '/' => MapPiece::MirrorTopRight,
            _ => panic!("Invalid map piece: {}", value),
        }
    }
}

impl From<MapPiece> for char {
    fn from(value: MapPiece) -> Self {
        match value {
            MapPiece::Empty => '.',
            MapPiece::SplitterH => '-',
            MapPiece::SplitterV => '|',
            MapPiece::MirrorTopLeft => '\\',
            MapPiece::MirrorTopRight => '/',
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'puzzle> {
    pub raw_lines: Vec<&'puzzle str>,
    pub optical_grid: Grid<MapPiece>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let puzzle_input = separated_list1(newline, is_a(".|\\-/"));

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, map)| PuzzleInput {
                raw_lines,
                optical_grid: Grid::new_from_iter(
                    map[0].len(),
                    map.len(),
                    map.iter().flat_map(|line| line.chars()),
                ),
            },
        ),
        multispace0,
    );

    parser(input)
}

impl<'puzzle> TryFrom<&'puzzle str> for PuzzleInput<'puzzle> {
    type Error = Error<&'puzzle str>;

    fn try_from(s: &'puzzle str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[rstest]
    #[case::test_input(TEST_INPUT)]
    #[case::final_input(INPUT)]
    /// Verifies that the test input is valid.
    fn test_puzzle_from_inputs(#[case] input: &str) {
        if !input.is_empty() {
            let input = PuzzleInput::try_from(input).unwrap();
            assert!(!input.raw_lines.is_empty());
            assert_eq!(input.raw_lines.len(), input.optical_grid.height() as usize);
        }
    }

    #[test]
    fn test_puzzle_from_input() {
        let input = TEST_INPUT;
        if input.is_empty() {
            return;
        };
        let input = PuzzleInput::try_from(input).unwrap();
        assert!(!input.raw_lines.is_empty());
        assert_eq!(input.optical_grid.height(), 10);
        assert_eq!(input.optical_grid.width(), 10);
    }

    #[test]
    /// Verifes that invalid input is rejected.
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        input.unwrap_err();
    }
}
