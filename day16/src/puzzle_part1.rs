use std::collections::{HashSet, VecDeque};

use aoc_grid::{Coordinate, Direction, Grid};

use crate::puzzle_input::{MapPiece, PuzzleInput};

/// Illuminate the optical grid, starting at the given position and direction.
/// Returns a grid of bools, where true indicates that the cell is energized.
pub fn illuminate(
    input: &PuzzleInput,
    initial_position: Coordinate,
    initial_direction: Direction,
) -> Grid<bool> {
    let mut energized = input.optical_grid.map(|_| false);
    let mut visited = HashSet::<(Coordinate, Direction)>::new();

    let mut to_visit = VecDeque::from(vec![(initial_position, initial_direction)]);
    while let Some((position, direction)) = to_visit.pop_front() {
        if !input.optical_grid.coordinate_valid(position) {
            // We've left the grid, so mo more work required.
            continue;
        }

        if visited.contains(&(position, direction)) {
            // We've already visited this position from this direction, so we're done.
            continue;
        }

        // Mark this position as visited.
        visited.insert((position, direction));

        // We've not visited this position from this direction, so we need to energize it.
        energized[position] = true;

        match input.optical_grid[position] {
            MapPiece::Empty => {
                // We've hit an empty space, so we just continue in the same direction.
                to_visit.push_back((position + direction, direction));
                continue;
            }
            MapPiece::SplitterV => match direction {
                Direction::Left | Direction::Right => {
                    to_visit.push_back((position + (0, -1), Direction::Up));
                    to_visit.push_back((position + (0, 1), Direction::Down));
                }
                _ => to_visit.push_back((position + direction, direction)),
            },
            MapPiece::SplitterH => match direction {
                Direction::Up | Direction::Down => {
                    to_visit.push_back((position + (-1, 0), Direction::Left));
                    to_visit.push_back((position + (1, 0), Direction::Right));
                }
                _ => to_visit.push_back((position + direction, direction)),
            },
            MapPiece::MirrorTopLeft => {
                let new_dir = match direction {
                    Direction::Up => Direction::Left,
                    Direction::Right => Direction::Down,
                    Direction::Down => Direction::Right,
                    Direction::Left => Direction::Up,
                    _ => unreachable!("Invalid direction for mirror"),
                };
                to_visit.push_back((position + new_dir, new_dir));
            }
            MapPiece::MirrorTopRight => {
                let new_dir = match direction {
                    Direction::Up => Direction::Right,
                    Direction::Right => Direction::Up,
                    Direction::Down => Direction::Left,
                    Direction::Left => Direction::Down,
                    _ => unreachable!("Invalid direction for mirror"),
                };
                to_visit.push_back((position + new_dir, new_dir));
            }
        }
    }

    energized
}

pub fn solve(input: &PuzzleInput) -> String {
    let energized = illuminate(input, Coordinate::new(0, 0), Direction::Right);
    energized.iter().filter(|&e| *e).count().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    // #[ignore]
    fn test_solve_actual() {
        let input = PuzzleInput::try_from(INPUT).unwrap();
        assert_eq!(solve(&input), "8112");
    }
    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "46");
    }
}
