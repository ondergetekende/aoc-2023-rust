use aoc_grid::{Coordinate, Direction};

use crate::{puzzle_input::PuzzleInput, puzzle_part1::illuminate};

pub fn solve(input: &PuzzleInput) -> String {
    let mut initial_positions = Vec::<(Coordinate, Direction)>::new();

    initial_positions.extend(
        (0..input.optical_grid.width()).map(|col| (Coordinate::new(col, 0), Direction::Down)),
    );
    initial_positions.extend((0..input.optical_grid.width()).map(|col| {
        (
            Coordinate::new(col, input.optical_grid.height() - 1),
            Direction::Up,
        )
    }));
    initial_positions.extend(
        (0..input.optical_grid.height()).map(|row| (Coordinate::new(row, 0), Direction::Right)),
    );
    initial_positions.extend((0..input.optical_grid.height()).map(|row| {
        (
            Coordinate::new(row, input.optical_grid.width() - 1),
            Direction::Left,
        )
    }));

    initial_positions
        .iter()
        .map(|(pos, dir)| illuminate(input, *pos, *dir).iter().filter(|&&e| e).count())
        .max()
        .expect("")
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "51");
    }
}
