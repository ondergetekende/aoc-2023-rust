use crate::puzzle_input::*;

pub fn solve(input: &PuzzleInput) -> String {
    input
        .seeds
        .iter()
        .map(|seed| {
            // For each category, try to map the seed
            input.category_maps.iter().fold(*seed, |c, category_map| {
                // Try to map the seed
                category_map
                    .seed_maps
                    .iter()
                    .find_map(|seed_map| {
                        if (c < seed_map.origin) || (c >= seed_map.origin + seed_map.count) {
                            // Seed is not in this seed map
                            None
                        } else {
                            Some(c + seed_map.destination - seed_map.origin)
                        }
                    })
                    // Fallback when unmapped
                    .unwrap_or(c)
            })
        })
        .min()
        .unwrap()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve_part_1() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "35");
    }
}
