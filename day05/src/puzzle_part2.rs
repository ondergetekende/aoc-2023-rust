use crate::puzzle_input::*;
use num::clamp;
use std::cmp::{max, min};

pub fn solve(input: &PuzzleInput) -> String {
    // Convert the seed ranges into a set of mappings mapping to themselves
    // This yields a list of mapping sets (each representing the stages in the input)
    // first entry is an identity mapping for the seed ranges. That way we don't need a special case for the first stage.
    let mappings = from_input(input);

    // Chain each of the mappings together. Chaining leaves us with a map that would do exactly the same as the two originals
    // called after each other.
    let result = mappings
        .iter()
        .fold(mappings[0].clone(), |old_mappings, new_mappings| {
            old_mappings.chain(new_mappings)
        });

    // Find the lowest number in the destination ranges, this is the answer
    result
        .iter()
        .map(|m| m.destination)
        .min()
        .unwrap()
        .to_string()
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct RangeMapping {
    destination: i64,
    origin: i64,
    length: i64,
}

impl RangeMapping {
    /// Quick constructor for use in tests
    fn _new(destination: i64, origin: i64, length: i64) -> Self {
        Self {
            destination,
            origin,
            length,
        }
    }

    /// Helper function to check if v is a valid value for this mapping.
    fn origin_contains(&self, v: i64) -> bool {
        v >= self.origin && v < self.origin + self.length
    }

    /// Creates a new mapping that applies some of the mapping from self to other.
    /// The resulting rule won't handle any values that wouldn't have passed through both mappings.
    /// To get a complete rule, you'll to combine the original and the new rule.
    ///
    /// if other.contains(self.apply(x))
    ///     self.chain(other).apply(x) == other.apply(self.apply(x))
    fn chain(&self, other: &RangeMapping) -> Self {
        let start = max(self.destination, other.origin);
        let end = min(self.destination + self.length, other.origin + other.length);

        RangeMapping {
            origin: self.inverse(start),
            length: end - start,
            destination: other.apply(start),
        }
    }

    /// Applies the mapping to v, includes the fallback.
    fn apply(&self, v: i64) -> i64 {
        if v < self.origin || v >= self.origin + self.length {
            // Outside of the range, return original value as fallback
            v
        } else {
            // Inside the range, apply the mapping
            self.destination + v - self.origin
        }
    }

    // Inverts the mapping, includes the fallback.
    fn inverse(&self, position: i64) -> i64 {
        if position < self.destination || position >= self.destination + self.length {
            // Outside of the range, return original value as fallback
            position
        } else {
            // Inside the range, unapply the mapping
            position + self.origin - self.destination
        }
    }

    /// Turns this mapping into a set of mappings that don't handle the range described by other.
    fn remove_shadowed(&self, shadower: &Self) -> Vec<Self> {
        // Empty range can be dropped altogether
        if self.length <= 0 {
            return vec![];
        }

        // If other is empty, it won't shadow anything
        if shadower.length <= 0 {
            return vec![*self];
        }

        // Between the ranges, there are 4 useful points.
        // If we generate the ranges between them, we cover all of the possible intersections.
        // Some of those possibilities will be empty, we'll drop those.
        // Some of these points may be outside of the original range, so we'll clamp them to be within the range.
        let start = self.origin;
        let end = self.origin + self.length;

        let mut cut_points = [
            start,
            end,
            clamp(shadower.origin, start, end),
            clamp(shadower.origin + shadower.length, start, end),
        ];
        cut_points.sort();

        cut_points
            .windows(2)
            .filter_map(|w| {
                let start = w[0];
                let end = w[1];

                if start == end || shadower.origin_contains(start) {
                    // Empty range, or we're inside the shadowed range
                    None
                } else {
                    Some(RangeMapping {
                        origin: start,
                        length: end - start,
                        destination: self.apply(w[0]),
                    })
                }
            })
            .collect()
    }
}

trait RangeMappingSetTrait {
    fn apply(&self, position: i64) -> i64;
    fn chain(&self, other: &Self) -> Self;
    fn remove_shadows(&self) -> Self;
}

impl RangeMappingSetTrait for Vec<RangeMapping> {
    // Applies the mappings to the position, includes the fallback.
    fn apply(&self, position: i64) -> i64 {
        self.iter()
            .find_map(|m| {
                if m.origin_contains(position) {
                    Some(m.apply(position))
                } else {
                    None
                }
            })
            // Fallback if none of the mappings apply
            .unwrap_or(position)
    }

    /// Chains two sets of mappings together.
    fn chain(&self, new_mappings: &Vec<RangeMapping>) -> Vec<RangeMapping> {
        let mut result = Vec::new();

        // Add mappings for the old mappings chained with the new mappings.
        // This covers everything that was handled by the old mappings, and the new mappings,
        // but it won't handle anything the new mappings didn't handle.
        result.extend(
            self.iter()
                .flat_map(|old_rule| new_mappings.iter().map(|new_rule| old_rule.chain(new_rule))),
        );

        // Add the old mappings, to handle the fallthrough cases.
        result.extend(self.iter());

        // May of the old mappings have been shadowed by the new mappings.
        // We need to remove those, as in the end we'll inspect the ranges, and shadowed ranges will give false outputs.
        result.remove_shadows()
    }

    // Removes all mappings that are shadowed by earlier mappings
    fn remove_shadows(&self) -> Vec<RangeMapping> {
        self.iter().fold(Vec::new(), |mut result, mapping| {
            let mut tmp = vec![*mapping];

            result.iter().for_each(|other| {
                tmp = tmp
                    .iter()
                    .flat_map(|m| m.remove_shadowed(other))
                    .collect::<Vec<_>>();
            });

            result.extend(tmp.iter());
            result
        })
    }
}

/// Returns a set of ranges that represents the input.
/// Initial seed list is represented by identity mappings (destination == origin)
fn from_input(input: &PuzzleInput) -> Vec<Vec<RangeMapping>> {
    // Add the seed ranges as the first set of mappings
    // These map to themselves.
    let mut result = vec![input
        .seed_ranges
        .iter()
        .map(|(seed, count)| RangeMapping {
            destination: *seed as i64,
            origin: *seed as i64,
            length: *count as i64,
        })
        .collect()];

    result.extend(
        input
            // Translate the category maps to a set of range mappings
            .category_maps
            .iter()
            .map(|category_map| {
                category_map
                    .seed_maps
                    .iter()
                    .map(|sm| RangeMapping {
                        destination: sm.destination as i64,
                        origin: sm.origin as i64,
                        length: sm.count as i64,
                    })
                    .collect::<Vec<_>>()
            }),
    );

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_apply() {
        let range = RangeMapping::_new(100, 10, 10);
        assert_eq!(range.apply(1), 1);
        assert_eq!(range.apply(10), 100);
        assert_eq!(range.apply(19), 109);
        assert_eq!(range.apply(20), 20);
    }

    #[test]
    fn test_follow_example_1() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        let mappings = from_input(&input);

        let route = mappings.iter().fold(vec![82], |mut acc, mapping| {
            let idx = *acc.iter().last().unwrap();
            let next = mapping
                .iter()
                .find_map(|m| {
                    if m.origin_contains(idx) {
                        Some(m.apply(idx))
                    } else {
                        None
                    }
                })
                .unwrap_or(idx);
            acc.push(next);
            acc
        });
        assert_eq!(route, vec![82, 82, 84, 84, 84, 77, 45, 46, 46]);
    }

    /// ### Remove shadows ###

    #[test]
    fn test_remove_shadows() {
        let mappings = vec![
            RangeMapping::_new(0, 0, 30),
            RangeMapping::_new(110, 10, 10),
        ];

        assert_eq!(
            mappings[0].remove_shadowed(&mappings[1]),
            vec![RangeMapping::_new(0, 0, 10), RangeMapping::_new(20, 20, 10),],
        );

        let result = mappings.remove_shadows();
        assert_eq!(result, vec![RangeMapping::_new(0, 0, 30),]);
    }

    /// ########### Solving ###########

    #[test]
    // #[ignore]
    fn test_solve_part_2() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "46");
    }

    #[test]
    fn test_solve_part_trivial() {
        let input = PuzzleInput::try_from(
            "
seeds: 10 10

a-to-b map:
15 5 10
",
        )
        .unwrap();

        // Map would be:
        // src: 10 11 12 13 14 15 16 17 18 19
        // dst: 15 16 17 18 19 15 16 17 18 19
        assert_eq!(solve(&input), "15");
    }

    #[test]
    fn test_solve_part_trivial_2() {
        let input = PuzzleInput::try_from(
            "seeds: 10 10

a-to-b map:
15 5 10
25 5 10

b-to-c map:
5 16 2
",
        )
        .unwrap();

        // Map would be:
        // start  : 10 11 12 13 14 15 16 17 18 19
        // stage 1: 15 16 17 18 19 15 16 17 18 19
        // stage 2: 15  5  6 18 19 15  5  6 18 19
        assert_eq!(solve(&input), "5");
    }
}
