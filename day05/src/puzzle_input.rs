use nom::{
    self,
    bytes::complete::tag,
    character::complete::{self, alpha1, multispace0, multispace1},
    combinator::map,
    error::Error,
    multi::separated_list1,
    sequence::{delimited, separated_pair, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub seeds: Vec<u64>,
    pub seed_ranges: Vec<(u64, u64)>,
    pub category_maps: Vec<CategoryMap<'a>>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct CategoryMap<'a> {
    pub source_category: &'a str,
    pub target_category: &'a str,
    pub seed_maps: Vec<SeedMap>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct SeedMap {
    pub origin: u64,
    pub destination: u64,
    pub count: u64,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let seed_map = map(
        tuple((
            complete::u64,
            delimited(multispace0, complete::u64, multispace0),
            complete::u64,
        )),
        |(destination, origin, count)| SeedMap {
            destination,
            origin,
            count,
        },
    );

    let category_map = map(
        tuple((
            separated_pair(alpha1, tag("-to-"), alpha1),
            delimited(multispace0, tag("map:"), multispace1),
            separated_list1(multispace1, seed_map),
        )),
        |((source_category, target_category), _, seed_maps)| CategoryMap {
            source_category,
            target_category,
            seed_maps,
        },
    );

    let mut parser = map(
        delimited(
            multispace0,
            tuple((
                delimited(
                    tag("seeds: "),
                    separated_list1(multispace1, complete::u64),
                    multispace1,
                ),
                separated_list1(multispace1, category_map),
            )),
            multispace0,
        ),
        |(seeds, category_maps)| PuzzleInput {
            seeds: seeds.clone(),
            seed_ranges: seeds
                .clone()
                .as_slice()
                .chunks(2)
                .map(|r| (r[0], r[1]))
                .collect(),
            category_maps,
        },
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(input.seeds, vec![79, 14, 55, 13]);
        assert_eq!(input.seed_ranges, vec![(79, 14), (55, 13)]);
        assert_eq!(input.category_maps.len(), 7);
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        PuzzleInput::try_from(INPUT).unwrap();
    }
    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
