use crate::puzzle_part1::calculate_hash;

use crate::puzzle_input::PuzzleInput;

pub fn solve(input: &PuzzleInput) -> String {
    let mut boxes = (0..256)
        .map(|_| Vec::<(&str, usize)>::new())
        .collect::<Vec<_>>();

    for instruction in &input.steps {
        if let Some(label) = instruction.strip_suffix('-') {
            let lenses = &mut boxes[calculate_hash(label) as usize];

            // Remove the lens from the box
            if let Some(index) = lenses.iter().position(|&(l, _)| l == label) {
                lenses.remove(index);
            }
        } else {
            debug_assert!(instruction.contains('='));
            let (label, strength) = instruction.split_once('=').unwrap();
            let strength = strength.parse::<usize>().unwrap();

            let lenses = &mut boxes[calculate_hash(label) as usize];

            if let Some(index) = lenses.iter().position(|&(l, _)| l == label) {
                // Replace current lens with new lens
                lenses[index] = (label, strength);
            } else {
                // Add new lens to end of the box
                lenses.push((label, strength));
            }
        }
    }

    // Multiply the box contents together, according to the puzzle instructions.
    boxes
        .iter()
        .enumerate()
        .map(|(i, lenses)| {
            lenses
                .iter()
                .enumerate()
                .map(|(offset, (_label, strength))| (offset + 1) * strength)
                .sum::<usize>()
                * (i + 1)
        })
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "145");
    }
}
