use nom::{
    self,
    bytes::complete::{is_not, tag},
    character::complete::{line_ending, multispace0, not_line_ending},
    combinator::{map, peek},
    error::Error,
    multi::separated_list1,
    sequence::{delimited, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub raw_lines: Vec<&'a str>,
    pub steps: Vec<&'a str>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let puzzle_input = separated_list1(tag(","), is_not(","));

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, steps)| PuzzleInput { raw_lines, steps },
        ),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert!(!input.raw_lines.is_empty());
        assert_eq!(input.steps.len(), 11);
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT).unwrap();
        assert!(!input.raw_lines.is_empty());
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
