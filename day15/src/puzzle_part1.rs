use crate::puzzle_input::*;

pub fn calculate_hash(input: &str) -> u8 {
    input
        .bytes()
        .fold(0u8, |state, byte| state.wrapping_add(byte).wrapping_mul(17))
}

pub fn solve(input: &PuzzleInput) -> String {
    input
        .steps
        .iter()
        .map(|step| calculate_hash(step) as u32)
        .sum::<u32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[rstest]
    #[case("rn=1", 30)]
    #[case("cm-", 253)]
    #[case("qp=3", 97)]
    #[case("cm=2", 47)]
    #[case("qp-", 14)]
    #[case("pc=4", 180)]
    #[case("ot=9", 9)]
    #[case("ab=5", 197)]
    #[case("pc-", 48)]
    #[case("pc=6", 214)]
    #[case("ot=7", 231)]
    fn test_hash(#[case] input: &str, #[case] expected: u8) {
        assert_eq!(calculate_hash(input), expected);
    }

    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "1320");
    }
}
