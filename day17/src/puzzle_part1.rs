use crate::puzzle_input::PuzzleInput;
use aoc_grid::{Coordinate, Direction, Grid};
use pathfinding::prelude::astar;

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
struct State<'puzzle> {
    grid: &'puzzle Grid<u8>,
    pos: Coordinate,
    consecutive_steps: usize,
    consecutive_steps_direction: Direction,
}

impl State<'_> {
    fn successors(&self) -> Vec<(Self, usize)> {
        let result: Vec<(Self, usize)> = Direction::CARDINAL_4
            .iter()
            // filter out 180deg turn
            .filter(|&&d| self.consecutive_steps_direction != d.opposite())
            // filter out whatever would lead to 3 consecutive steps
            .filter(|&&d| self.consecutive_steps < 3 || self.consecutive_steps_direction != d)
            // Generate new states
            .filter_map(|&d| -> Option<(State<'_>, usize)> {
                let new_pos = self.pos + d;
                // println!("{:?} {:?}", self.pos, d);
                let loss = *self.grid.get(new_pos)?;

                let samedir = self.consecutive_steps_direction == d;

                Some((
                    Self {
                        grid: self.grid,
                        pos: new_pos,
                        consecutive_steps: if samedir {
                            self.consecutive_steps + 1
                        } else {
                            1
                        },
                        consecutive_steps_direction: d,
                    },
                    loss as usize,
                ))
            })
            .collect();
        result
    }
}

fn _visualize_path(path: &[State], heat_map: &Grid<u8>) -> String {
    let mut grid = Grid::<u8>::new(
        (path.iter().map(|p| p.pos.0).max().unwrap() + 1) as usize,
        (path.iter().map(|p| p.pos.1).max().unwrap() + 1) as usize,
    );

    for p in path {
        grid[p.pos] = p.consecutive_steps as u8;
    }

    let heat_path_map = grid.zip(
        heat_map,
        |(&a, &b)| if a > 0 { (b'0' + b) as char } else { '.' },
    );

    heat_path_map.to_string()
}

pub fn solve(input: &PuzzleInput) -> String {
    let start_pos = State {
        grid: &input.heat_loss_map,
        pos: Coordinate::new(0, 0),
        consecutive_steps: 0,
        consecutive_steps_direction: Direction::Right,
    };

    let goal = Coordinate::new(
        input.heat_loss_map.width() - 1,
        input.heat_loss_map.height() - 1,
    );

    let result = astar(
        &start_pos,
        |p| p.successors(),
        |p| p.pos.manhattan_distance(goal),
        |p| p.pos == goal,
    )
    .expect("There is always a valid path");

    // println!("{}", _visualize_path(&result.0, &input.heat_loss_map));

    result.1.to_string()
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[rstest]
    #[case::test_input("341\n321", "6")]
    #[case::test_input("241343\n321545", "20")]
    // #[case::test_input("11323\n35623\n54254\n45452\n67536", "32")]
    fn test_trivial(#[case] input: &str, #[case] expected: &str) {
        let input = PuzzleInput::try_from(input).unwrap();
        assert_eq!(solve(&input), expected);
    }

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(include_str!("../test_input.txt")).unwrap();
        assert_eq!(solve(&input), "102");
    }

    #[test]
    #[ignore]
    fn test_actual_solve() {
        // assert!(false);
        let input = PuzzleInput::try_from(include_str!("../input.txt")).unwrap();
        assert_eq!(solve(&input), "686");
    }
}
