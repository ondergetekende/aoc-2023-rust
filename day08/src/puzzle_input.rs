use std::collections::BTreeMap;

use nom::{
    self,
    bytes::complete::{is_a, is_not, tag},
    character::complete::{line_ending, multispace0, newline, not_line_ending},
    combinator::{map, peek},
    error::Error,
    multi::separated_list1,
    sequence::{delimited, separated_pair, terminated, tuple},
    Finish, IResult,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub raw_lines: Vec<&'a str>,
    pub directions: &'a str,
    pub nodes: BTreeMap<&'a str, Node<'a>>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Node<'a> {
    pub left: &'a str,
    pub right: &'a str,
}

const ALPHABETH: &str = "12ABCDEFGHIJKLMNOPQRSTUVWXYZ";

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let label = || is_a(ALPHABETH);

    let puzzle_input = separated_pair(
        map(is_not("\n"), |s: &str| s.trim()),
        tag("\n\n"),
        map(
            separated_list1(
                newline,
                terminated(
                    separated_pair(
                        label(),
                        tag(" = ("),
                        map(
                            separated_pair(label(), tag(", "), label()),
                            |(left, right)| Node { left, right },
                        ),
                    ),
                    tag(")"),
                ),
            ),
            |a| a.into_iter().collect::<BTreeMap<_, _>>(),
        ),
    );

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, (directions, nodes))| PuzzleInput {
                raw_lines,
                directions,
                nodes,
            },
        ),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const TEST_INPUT2: &str = include_str!("../test_input2.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(input.directions, "RL");

        assert_eq!(input.nodes.len(), 7);
        assert_eq!(
            input.nodes["GGG"],
            Node {
                left: "GGG",
                right: "GGG"
            }
        );
    }
    #[test]
    fn test_puzzle_input_from_test_input2() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT2);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
