use std::iter::{from_fn, repeat};

use num::Integer;

use crate::puzzle_input::*;

/// Returns a never-ending iterator that represents the path taken by a ghost.
fn follow_route<'a>(input: &'a PuzzleInput, start: &'a str) -> impl Iterator<Item = &'a str> {
    let mut current_location = start;
    let mut directions = repeat(input.directions).flat_map(|s| s.chars());

    from_fn(move || {
        let d = directions.next()?;
        current_location = match d {
            'L' => input.nodes[current_location].left,
            'R' => input.nodes[current_location].right,
            _ => unreachable!(),
        };
        Some(current_location)
    })
}

fn find_period(start: &str, input: &PuzzleInput) -> usize {
    follow_route(input, start)
        .enumerate()
        // When we find the exit, we know that the loop ends next.
        .find_map(|(idx, n)| {
            if n.ends_with('Z') {
                Some(idx + 1)
            } else {
                None
            }
        })
        .unwrap()
}

pub fn solve(input: &PuzzleInput) -> String {
    let common_loop_point = input
        .nodes
        .iter()
        // Find the loop periods for each of the starting nodes
        .filter_map(|(k, _)| match k.ends_with('A') {
            true => Some(find_period(k, input)),
            false => None,
        })
        // find the lowest common multiple of all the periods
        .fold(1, |acc, x| acc.lcm(&x));

    // Each loop has an exit just before the loop starts.
    // Our answer points to that exit.
    (common_loop_point).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const REAL_INPUT: &str = include_str!("../input.txt");
    const TEST_INPUT: &str = include_str!("../test_input3.txt");

    #[test]
    fn test_input_assumptions() {
        let input = PuzzleInput::try_from(REAL_INPUT).unwrap();

        let start_locations = input
            .nodes
            .iter()
            .filter_map(|(k, _)| match k.ends_with('A') {
                true => Some(*k),
                false => None,
            })
            .collect::<Vec<_>>();

        for loc in start_locations {
            _test_input_assumptions(&input, loc);
        }
    }

    fn _test_input_assumptions(input: &PuzzleInput, start: &str) {
        println!("Testing input assumptions for start point {} ", start);
        let first_4_exits = follow_route(input, start)
            .enumerate()
            .filter_map(|(idx, n)| if n.ends_with('Z') { Some(idx) } else { None })
            .take(4)
            .collect::<Vec<_>>();

        // assert_eq!(first_4_exits[0] % (input.directions.len()+1), 0);

        // This proves a fact that's not mentioned in the puzzle description:
        // All startpoints loop with a single length, starting at the start point,
        // and ending at the exit.
        let period = first_4_exits[1] - first_4_exits[0];
        assert_eq!(period, first_4_exits[2] - first_4_exits[1]);
        assert_eq!(period, first_4_exits[3] - first_4_exits[2]);

        // Verify that the last entry before the loop is an exit
        assert_eq!(
            follow_route(input, start)
                .nth(period - 1)
                .unwrap()
                .chars()
                .last()
                .unwrap(),
            'Z'
        );
        assert_eq!(
            follow_route(input, start)
                .nth(period * 2 - 1)
                .unwrap()
                .chars()
                .last()
                .unwrap(),
            'Z'
        );
        assert_eq!(
            follow_route(input, start)
                .nth(period * 3 - 1)
                .unwrap()
                .chars()
                .last()
                .unwrap(),
            'Z'
        );
    }

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "6");
    }
}
