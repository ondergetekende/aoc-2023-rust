use std::iter::repeat;

use crate::puzzle_input::*;

pub fn solve(input: &PuzzleInput) -> String {
    let start = "AAA";
    let destination = "ZZZ";

    let mut current_location = start;

    let steps = repeat(input.directions)
        .flat_map(|d| d.chars())
        .map(move |direction| {
            current_location = match direction {
                'L' => input.nodes[current_location].left,
                'R' => input.nodes[current_location].right,
                _ => unreachable!(),
            };
            current_location
        })
        .take_while(|&loc| loc != destination)
        .collect::<Vec<_>>();

    // +1 because we don't count the last step
    (steps.len() + 1).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const TEST_INPUT2: &str = include_str!("../test_input2.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "2");
    }
    #[test]
    fn test_solve2() {
        let input = PuzzleInput::try_from(TEST_INPUT2).unwrap();
        assert_eq!(solve(&input), "6");
    }
}
