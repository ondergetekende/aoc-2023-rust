use nom::{
    self,
    bytes::complete::tag,
    character::complete::{self, multispace0, multispace1, newline},
    combinator::map,
    error::Error,
    multi::separated_list1,
    sequence::{delimited, tuple},
    Finish, IResult, Parser,
};

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput {
    pub cards: Vec<Card>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Card {
    pub index: u8,
    pub winning: Vec<u8>,
    pub actual: Vec<u8>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let line = tuple((
        delimited(
            tuple((tag("Card"), multispace1)),
            complete::u8,
            tuple((tag(":"), multispace0)),
        ),
        separated_list1(multispace1, complete::u8),
        tuple((multispace1, tag("|"), multispace1)),
        separated_list1(multispace1, complete::u8),
    ))
    .map(|(index, winning, _, actual)| Card {
        index,
        winning,
        actual,
    });

    let mut parser = delimited(
        multispace0,
        map(separated_list1(newline, line), |cards| PuzzleInput {
            cards,
        }),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_simple() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from("Card  1: 1 | 2").unwrap();
        assert_eq!(input.cards.len(), 1);
        assert_eq!(input.cards[0].index, 1);
        assert_eq!(input.cards[0].winning, vec![1]);
        assert_eq!(input.cards[0].actual, vec![2]);
    }

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(input.cards.len(), 6);
        assert_eq!(input.cards[0].index, 1);
        assert_eq!(input.cards[0].winning, vec![41, 48, 83, 86, 17]);
        assert_eq!(input.cards[0].actual, vec![83, 86, 6, 31, 17, 9, 48, 53]);
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        PuzzleInput::try_from(INPUT).unwrap();
    }
}
