use crate::puzzle_input::*;

pub fn solve_part_1(input: &PuzzleInput) -> String {
    input
        .cards
        .iter()
        .map(|card| {
            let matching_numbers = card
                .actual
                .iter()
                .filter(|a| card.winning.contains(a))
                .count();
            if matching_numbers == 0 {
                return 0;
            }
            2i32.pow(matching_numbers as u32 - 1)
        })
        .sum::<i32>()
        .to_string()
}

pub fn solve_part_2(input: &PuzzleInput) -> String {
    let cards_with_scores = input.cards.iter().map(|card| {
        (
            card,
            card.actual
                .iter()
                .filter(|a| card.winning.contains(a))
                .count(),
        )
    });

    let mut card_copies = input.cards.iter().map(|_| 1).collect::<Vec<_>>();

    for (card, score) in cards_with_scores {
        let card_idx = card.index as usize - 1;
        let current_card_copies = card_copies[card_idx];
        for offset in 1..=score {
            card_copies[card_idx + offset] += current_card_copies;
        }
    }

    card_copies.iter().sum::<usize>().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve_part_1() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve_part_1(&input), "13");
    }

    #[test]
    fn test_solve_part_2() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve_part_2(&input), "30");
    }
}
