use core::fmt;
use std::{
    fmt::Debug,
    ops::{Deref, Index},
};

use crate::grid::Grid;

// ----------------------------------------------------------------------------------------

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct GridCell<'grid, T> {
    pub grid: &'grid Grid<T>,
    pub coordinate: (isize, isize),
}

impl<'grid, T> GridCell<'grid, T> {
    pub fn new<U: Into<(isize, isize)>>(grid: &'grid Grid<T>, origin: U) -> Self {
        Self {
            grid,
            coordinate: origin.into(),
        }
    }

    pub fn set_coordinate(&mut self, coordinate: (isize, isize)) {
        self.coordinate = coordinate;
    }

    #[must_use]
    pub fn is_valid(&self) -> bool {
        self.grid.coordinate_valid(self.coordinate)
    }

    #[must_use]
    pub fn get(&self) -> Option<&T> {
        self.grid.get(self.coordinate)
    }

    #[must_use]
    pub fn goto<U: Into<(isize, isize)>>(&self, coordinate: U) -> Self {
        Self {
            grid: self.grid,
            coordinate: coordinate.into(),
        }
    }
}

impl<'grid, T> Deref for GridCell<'grid, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.grid[self.coordinate]
    }
}

impl<T> Debug for GridCell<'_, T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "({}, {}) {:?}",
            self.coordinate.0,
            self.coordinate.1,
            self.get()
        )?;
        Ok(())
    }
}

// Allow indexing with a coordinate offset.
impl<'grid, T, U> Index<U> for GridCell<'grid, T>
where
    U: Into<(isize, isize)>,
{
    type Output = T;

    fn index(&self, pos: U) -> &'grid Self::Output {
        self.grid.get(pos.into()).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_grid_cell() {
        let mut grid = Grid::new(3, 3);
        grid[(1, 1)] = 42;
        let cell = grid.entry((1, 1));
        assert_eq!(cell.coordinate, (1, 1));
        assert_eq!(cell.get(), Some(&42));
    }
}
