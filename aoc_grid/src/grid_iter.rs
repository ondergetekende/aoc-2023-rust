use std::ops::{Index, IndexMut};

use crate::grid::Grid;

pub trait GridRange<'grid, T> {
    fn get(&self, index: isize) -> &T;
    fn len(&self) -> usize;

    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    fn iter(&'grid self) -> GridIter<'grid, T>
    where
        Self: Sized,
    {
        GridIter {
            range: self,
            start: 0,
            end: self.len() as isize,
        }
    }
}

pub trait GridRangeMut<'grid, T>: GridRange<'grid, T> {
    fn get_mut(&mut self, index: isize) -> &mut T;
}

// ======================================================================
// Implementation for Generalized iterator

pub struct GridIter<'grid, T> {
    range: &'grid dyn GridRange<'grid, T>,
    start: isize,
    end: isize,
}

impl<'grid, T> Iterator for GridIter<'grid, T> {
    type Item = &'grid T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.start < self.end {
            let result = self.range.get(self.start);
            self.start += 1;
            Some(result)
        } else {
            None
        }
    }
}

impl<'grid, T> ExactSizeIterator for GridIter<'grid, T> {
    fn len(&self) -> usize {
        (self.end - self.start) as usize
    }
}

impl<'grid, T> DoubleEndedIterator for GridIter<'grid, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.end > self.start {
            self.end -= 1;
            Some(self.range.get(self.end))
        } else {
            None
        }
    }
}

// ======================================================================
// Implementation for column

pub struct GridColumn<'grid, T> {
    grid: &'grid Grid<T>,
    x: isize,
}

impl<T> GridRange<'_, T> for GridColumn<'_, T> {
    fn get(&self, index: isize) -> &T {
        &self.grid[(self.x, index)]
    }

    fn len(&self) -> usize {
        self.grid.height() as usize
    }
}

impl<T> Index<isize> for GridColumn<'_, T> {
    type Output = T;

    fn index(&self, index: isize) -> &Self::Output {
        &self.grid[(self.x, index)]
    }
}

impl<'grid, T> GridColumn<'grid, T> {
    #[must_use]
    pub const fn new(grid: &'grid Grid<T>, x: isize) -> Self {
        Self { grid, x }
    }
}

pub struct GridColumnMut<'grid, T> {
    grid: &'grid mut Grid<T>,
    x: isize,
}

impl<'grid, T> GridColumnMut<'grid, T> {
    pub fn new(grid: &'grid mut Grid<T>, x: isize) -> Self {
        Self { grid, x }
    }
}
impl<T> GridRange<'_, T> for GridColumnMut<'_, T> {
    fn get(&self, index: isize) -> &T {
        &self.grid[(self.x, index)]
    }

    fn len(&self) -> usize {
        self.grid.height() as usize
    }
}

impl<T> GridRangeMut<'_, T> for GridColumnMut<'_, T> {
    fn get_mut(&mut self, index: isize) -> &mut T {
        &mut self.grid[(self.x, index)]
    }
}

impl<T> Index<isize> for GridColumnMut<'_, T> {
    type Output = T;

    fn index(&self, index: isize) -> &Self::Output {
        &self.grid[(self.x, index)]
    }
}

impl<T> IndexMut<isize> for GridColumnMut<'_, T> {
    fn index_mut(&mut self, index: isize) -> &mut Self::Output {
        &mut self.grid[(self.x, index)]
    }
}

// ======================================================================
// Implementation for row

pub struct GridRow<'grid, T> {
    grid: &'grid Grid<T>,
    y: isize,
}

impl<'grid, T> GridRow<'grid, T> {
    #[must_use]
    pub const fn new(grid: &'grid Grid<T>, y: isize) -> Self {
        Self { grid, y }
    }
}

impl<T> GridRange<'_, T> for GridRow<'_, T> {
    fn get(&self, index: isize) -> &T {
        &self.grid[(index, self.y)]
    }

    fn len(&self) -> usize {
        self.grid.width() as usize
    }
}

impl<T> Index<isize> for GridRow<'_, T> {
    type Output = T;

    fn index(&self, index: isize) -> &Self::Output {
        &self.grid[(index, self.y)]
    }
}

pub struct GridRowMut<'grid, T> {
    grid: &'grid mut Grid<T>,
    y: isize,
}

impl<'grid, T> GridRowMut<'grid, T> {
    pub fn new(grid: &'grid mut Grid<T>, y: isize) -> Self {
        Self { grid, y }
    }
}

impl<T> GridRange<'_, T> for GridRowMut<'_, T> {
    fn get(&self, index: isize) -> &T {
        &self.grid[(index, self.y)]
    }

    fn len(&self) -> usize {
        self.grid.width() as usize
    }
}

impl<T> GridRangeMut<'_, T> for GridRowMut<'_, T> {
    fn get_mut(&mut self, index: isize) -> &mut T {
        &mut self.grid[(index, self.y)]
    }
}

impl<T> Index<isize> for GridRowMut<'_, T> {
    type Output = T;

    fn index(&self, index: isize) -> &Self::Output {
        &self.grid[(index, self.y)]
    }
}

impl<T> IndexMut<isize> for GridRowMut<'_, T> {
    fn index_mut(&mut self, index: isize) -> &mut Self::Output {
        &mut self.grid[(index, self.y)]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_column() {
        let mut grid = Grid::new(3, 3);
        grid[(1, 1)] = 1;
        let column = grid.column(1);
        assert_eq!(column.len(), 3);
        assert_eq!(column[0], 0);
        assert_eq!(column[1], 1);
        assert_eq!(column[2], 0);
    }

    #[test]
    fn test_column_iter() {
        let mut grid = Grid::new(3, 3);
        grid[(1, 1)] = 1;
        let column = grid.column(1);
        let mut iter = column.iter();
        assert_eq!(iter.len(), 3);
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_row_iter_rev() {
        let mut grid = Grid::new(3, 3);
        grid[(1, 1)] = 1;
        grid[(1, 2)] = 2;
        let row = grid.column(1);
        let result = row.iter().rev().collect::<Vec<_>>();
        assert_eq!(result, vec![&2, &1, &0]);
    }
}
