use crate::puzzle_input::*;

fn extrapolate(nums: &[i64]) -> i64 {
    if nums.is_empty() {
        return 0;
    }

    if nums.iter().all(|&n| n == nums[0]) {
        return nums[0];
    }

    let deltas = nums.windows(2).map(|w| w[1] - w[0]).collect::<Vec<_>>();
    let final_delta = extrapolate(deltas.as_slice());

    nums[nums.len() - 1] + final_delta
}

pub fn solve(input: &PuzzleInput) -> String {
    input
        .raw_lines
        .iter()
        .map(|line| {
            let nums = line
                .trim()
                .split(' ')
                .map(|s| s.parse::<i64>().unwrap())
                .rev()
                .collect::<Vec<i64>>();

            extrapolate(&nums)
        })
        .sum::<i64>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "2");
    }
}
