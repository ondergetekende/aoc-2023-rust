use nom::{
    self,
    character::complete::{line_ending, multispace0, newline, not_line_ending, one_of},
    combinator::{map, peek},
    error::Error,
    multi::{many1, separated_list1},
    sequence::{delimited, tuple},
    Finish, IResult,
};

use aoc_grid::Grid;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Terrain {
    Ash,
    Lava,
}

impl From<Terrain> for char {
    fn from(terrain: Terrain) -> Self {
        match terrain {
            Terrain::Ash => '.',
            Terrain::Lava => '#',
        }
    }
}

impl From<char> for Terrain {
    fn from(c: char) -> Self {
        match c {
            '.' => Terrain::Ash,
            '#' => Terrain::Lava,
            _ => unreachable!("Invalid terrain: {}", c),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput<'a> {
    pub raw_lines: Vec<&'a str>,
    pub terrains: Vec<Grid<Terrain>>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let terrain_piece = map(one_of(".#"), |c: char| match c {
        '.' => Terrain::Ash,
        '#' => Terrain::Lava,
        _ => unreachable!("Invalid terrain: {}", c),
    });

    let terrain = map(
        separated_list1(line_ending, many1(terrain_piece)),
        Grid::from,
    );
    let puzzle_input = separated_list1(many1(newline), terrain);

    let mut parser = delimited(
        multispace0,
        map(
            tuple((
                peek(separated_list1(line_ending, not_line_ending)),
                puzzle_input,
            )),
            |(raw_lines, terrains)| PuzzleInput {
                raw_lines,
                terrains,
            },
        ),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput<'a> {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(input.raw_lines.len(), 15);
        assert_eq!(input.terrains.len(), 2);

        assert_eq!(input.terrains[0].width(), 9);
        assert_eq!(input.terrains[0].height(), 7);
    }

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
