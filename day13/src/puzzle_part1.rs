use aoc_grid::{Grid, GridRange};

use crate::puzzle_input::*;

fn find_reflection(grid: &Grid<Terrain>) -> Option<usize> {
    grid.rows()
        .fold(
            // Start with all possible split points.
            // We're ignoring spot 0, as it's the left edge of the grid, and we can't
            // actually see the reflection.
            (1..grid.width()).map(|x| x as usize).collect::<Vec<_>>(),
            |candidates, row| {
                candidates
                    .iter()
                    // Drop candidates for which the reflection doesn't match on this line.
                    .filter(|split_point| {
                        let row = row.iter().cloned().collect::<Vec<Terrain>>();
                        let (left, right) = row.split_at(**split_point);

                        left.iter()
                            .rev()
                            .zip(right.iter())
                            .all(|(left, right)| left == right)
                    })
                    .cloned()
                    .collect::<Vec<_>>()
            },
        )
        .iter()
        .cloned()
        .next()
}

pub fn solve(input: &PuzzleInput) -> String {
    input
        .terrains
        .iter()
        .map(|grid| {
            find_reflection(grid)
                .unwrap_or_else(|| find_reflection(&grid.transpose()).unwrap() * 100)
        })
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[rstest]
    #[case("##", Some(1))]
    #[case("##.\n#.#", None)]
    fn test_find_reflection(#[case] terrain_def: &str, #[case] expected: Option<usize>) {
        let PuzzleInput { terrains, .. } = PuzzleInput::try_from(terrain_def).unwrap();
        assert_eq!(find_reflection(&terrains[0]), expected);
    }

    #[test]
    fn test_partial_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(find_reflection(&input.terrains[0]), Some(5));
        assert_eq!(find_reflection(&input.terrains[1]), None);
        assert_eq!(find_reflection(&input.terrains[1].transpose()), Some(4));
    }

    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "405");
    }
}
