use aoc_grid::{Grid, GridRange};

use crate::puzzle_input::*;

fn find_reflection(grid: &Grid<Terrain>) -> Option<usize> {
    let mut result = None;
    for split_point in 1..grid.width() {
        // Count the number of mismatched cells in the reflection.
        let mismatch_count = grid
            .rows()
            .map(|row| {
                let row = row.iter().cloned().collect::<Vec<Terrain>>();
                let (left, right) = row.split_at(split_point as usize);

                left.iter()
                    .rev()
                    .zip(right.iter())
                    .filter(|(left, right)| left != right)
                    .count()
            })
            .sum::<usize>();

        if mismatch_count == 1 {
            // This would have matched if there was only one cell flipped.
            result = Some(split_point as usize);
        }
    }
    result
}

pub fn solve(input: &PuzzleInput) -> String {
    input
        .terrains
        .iter()
        .map(|grid| {
            find_reflection(grid)
                .unwrap_or_else(|| find_reflection(&grid.transpose()).unwrap() * 100)
        })
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_partial_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(find_reflection(&input.terrains[0]), None);
        assert_eq!(find_reflection(&input.terrains[0].transpose()), Some(3));
        assert_eq!(find_reflection(&input.terrains[1]), None);
        assert_eq!(find_reflection(&input.terrains[1].transpose()), Some(1));
    }

    #[test]
    // #[ignore]
    fn test_solve() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve(&input), "400");
    }
}
