use itertools::Itertools;

use crate::puzzle_input::*;

#[derive(Debug, PartialEq, Clone)]
struct Cell {
    entry: char,
    related: bool,
    value: i32,
}

fn is_symbol(c: char) -> bool {
    !matches!(c, '0'..='9' | '.')
}

pub fn solve_part_1(input: &PuzzleInput) -> String {
    // Create a working copy of the grid.
    let mut grid = input.grid.map(|entry| Cell {
        entry: *entry,
        related: false, // Will be set later
        value: 0,       // Will be set later
    });

    // Mark digits with adjacent symbols as related.
    for pos in grid.positions() {
        let is_symbol_adjacent = grid.iter_around(pos).any(|cell| is_symbol(cell.entry));
        if grid[pos].entry.is_ascii_digit() {
            // If any digit is adjacent to a symbol, mark this entry as related.
            grid[pos].related = is_symbol_adjacent;
        }
    }

    let s = grid.rows().flat_map(|row| {
        // Group into series of digits, by splitting on anything that isn't a digit.
        row.split(|cell| !cell.entry.is_ascii_digit())
            .filter_map(|entries| {
                if entries.is_empty() {
                    return None;
                }

                // If any of the digits are related, then the whole number is related.
                let is_related = entries.iter().any(|digit| digit.related);

                match is_related {
                    true => entries
                        .iter()
                        .map(|e| e.entry)
                        .collect::<String>()
                        .parse::<i32>()
                        .ok(),
                    false => None,
                }
            })
    });
    s.sum::<i32>().to_string()
}

pub fn solve_part_2(input: &PuzzleInput) -> String {
    // Create a working copy of the grid.
    let mut grid = input.grid.map(|entry| Cell {
        entry: *entry,
        related: false, // Will be set later
        value: 0,       // Will be set later
    });

    // Assign the numeric value to each of the cells.
    grid.rows_mut().for_each(|row| {
        row.split_mut(|cell| !cell.entry.is_ascii_digit())
            .filter(|c| !c.is_empty())
            .for_each(|digit_cells| {
                let value = digit_cells
                    .iter()
                    .map(|cell| cell.entry)
                    .collect::<String>()
                    .parse::<i32>()
                    .unwrap();
                digit_cells.iter_mut().for_each(|cell| cell.value = value)
            })
    });

    // Find cogs, and see if they connect to exactly two different numbers.
    grid.positions()
        .filter(|pos| grid[*pos].entry == '*')
        .filter_map(|pos| {
            let mut mesh_cogs = grid
                .iter_around(pos)
                .filter_map(|cell| match cell.value {
                    0 => None,
                    v => Some(v),
                })
                .unique(); // unique might nog be valud, as two different cogs could still have the same value.

            let cog1 = mesh_cogs.next();
            let cog2 = mesh_cogs.next();

            if let Some(c1val) = cog1 {
                if let Some(c2val) = cog2 {
                    // Found a cog meshing with two different numbers.
                    if mesh_cogs.next().is_none() {
                        // There isn't a third cog, so this is a valid cog.
                        return Some(c1val * c2val);
                    }
                }
            }
            None
        })
        .sum::<i32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");

    #[test]
    fn test_solve_part_1() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve_part_1(&input), "4361");
    }

    #[test]
    fn test_solve_part_2() {
        let input = PuzzleInput::try_from(TEST_INPUT).unwrap();
        assert_eq!(solve_part_2(&input), "467835");
    }
}
