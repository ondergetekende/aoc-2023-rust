use std::ops::{Index, IndexMut};

use crate::vector2d::Vector2D;

#[derive(Debug, PartialEq, Clone)]
pub struct Grid<T> {
    stride: usize,
    cells: Vec<T>,
}

impl<T> From<Vec<Vec<T>>> for Grid<T> {
    fn from(v: Vec<Vec<T>>) -> Self {
        let stride = v[0].len();

        if !v.iter().all(|row| row.len() == stride) {
            panic!("All rows must have the same length");
        }

        let cells = v.into_iter().flatten().collect();
        Self { stride, cells }
    }
}

impl<T> Index<Vector2D> for Grid<T> {
    type Output = T;

    fn index(&self, position: Vector2D) -> &Self::Output {
        assert!(self.valid_position(position));
        &self.cells[position.y as usize * self.stride + position.x as usize]
    }
}

impl<T> IndexMut<Vector2D> for Grid<T> {
    fn index_mut(&mut self, position: Vector2D) -> &mut Self::Output {
        assert!(self.valid_position(position));
        &mut self.cells[position.y as usize * self.stride + position.x as usize]
    }
}

impl<T> Grid<T> {
    pub fn iter_around(&self, origin: Vector2D) -> impl Iterator<Item = &T> + '_ {
        self.iter_pos_around(origin).map(|pos| &self[pos])
    }

    pub fn valid_position(&self, pos: Vector2D) -> bool {
        pos.x >= 0
            && pos.y >= 0
            && pos.x < self.stride as i32
            && pos.y < self.cells.len() as i32 / self.stride as i32
    }

    /// Generates up to 8 positions around the provided origin.
    /// Positions outside the grid are not returned.
    fn iter_pos_around(&self, origin: Vector2D) -> impl Iterator<Item = Vector2D> + '_ {
        origin
            .adjacent_positions()
            .filter(move |pos| self.valid_position(*pos))
    }

    pub fn map<U>(&self, f: impl Fn(&T) -> U) -> Grid<U> {
        let cells = self.cells.iter().map(f).collect();
        Grid {
            stride: self.stride,
            cells,
        }
    }

    /// Returns an iterator over all valid positions in the grid.
    pub fn positions(&self) -> impl Iterator<Item = Vector2D> {
        let height = self.cells.len() / self.stride;
        let width = self.stride;
        (0..height).flat_map(move |y| (0..width).map(move |x| Vector2D::new(x as i32, y as i32)))
    }

    pub fn rows(&self) -> impl Iterator<Item = &[T]> {
        self.cells.chunks(self.stride)
    }
    pub fn rows_mut(&mut self) -> impl Iterator<Item = &mut [T]> {
        self.cells.chunks_mut(self.stride)
    }
}
