use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Vector2D {
    pub x: i32,
    pub y: i32,
}

const ADJACENT_POSITIONS: [Vector2D; 8] = [
    Vector2D { x: -1, y: -1 },
    Vector2D { x: 0, y: -1 },
    Vector2D { x: 1, y: -1 },
    Vector2D { x: -1, y: 0 },
    Vector2D { x: 1, y: 0 },
    Vector2D { x: -1, y: 1 },
    Vector2D { x: 0, y: 1 },
    Vector2D { x: 1, y: 1 },
];

impl Vector2D {
    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    pub fn adjacent_positions(self) -> impl Iterator<Item = Vector2D> {
        ADJACENT_POSITIONS
            .iter()
            .map(move |pos| *pos + self)
            .filter(move |pos| pos.x >= 0 && pos.y >= 0)
    }
}

impl From<(i32, i32)> for Vector2D {
    fn from((x, y): (i32, i32)) -> Self {
        Self { x, y }
    }
}

impl Add for Vector2D {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl AddAssign for Vector2D {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs;
    }
}

impl Sub for Vector2D {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl SubAssign for Vector2D {
    fn sub_assign(&mut self, rhs: Self) {
        *self = *self - rhs;
    }
}

impl Mul<u32> for Vector2D {
    type Output = Self;

    fn mul(self, rhs: u32) -> Self::Output {
        Self::new(self.x * rhs as i32, self.y * rhs as i32)
    }
}

impl Mul<i32> for Vector2D {
    type Output = Self;

    fn mul(self, rhs: i32) -> Self::Output {
        Self::new(self.x * rhs, self.y * rhs)
    }
}

impl MulAssign<u32> for Vector2D {
    fn mul_assign(&mut self, rhs: u32) {
        *self = *self * rhs;
    }
}
