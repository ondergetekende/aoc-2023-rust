use nom::{
    self,
    bytes::complete::is_not,
    character::complete::{multispace0, newline},
    error::Error,
    multi::separated_list1,
    sequence::delimited,
    Finish, IResult, Parser,
};

use crate::grid::Grid;

#[derive(Debug, PartialEq, Clone)]
pub struct PuzzleInput {
    pub grid: Grid<char>,
}

fn parse_puzzle(input: &str) -> IResult<&str, PuzzleInput> {
    let line = is_not("\r\n").map(|s: &str| s.chars().collect::<Vec<_>>());
    let puzzle = separated_list1(newline, line);

    let mut parser = delimited(
        multispace0,
        puzzle.map(|entries| PuzzleInput {
            grid: Grid::from(entries),
        }),
        multispace0,
    );

    parser(input)
}

impl<'a> TryFrom<&'a str> for PuzzleInput {
    type Error = Error<&'a str>;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        match parse_puzzle(s).finish() {
            Ok((_remaining, puzzle_input)) => Ok(puzzle_input),
            Err(Error { input, code }) => Err(Error { input, code }),
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../test_input.txt");
    const INPUT: &str = include_str!("../input.txt");

    #[test]
    fn test_puzzle_input_from_input() {
        // Verifies that the main puzzle input is valid
        let input = PuzzleInput::try_from(INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_test_input() {
        // Verifies that the test puzzle input is valid
        let input = PuzzleInput::try_from(TEST_INPUT);
        assert!(input.is_ok());
    }

    #[test]
    fn test_puzzle_input_from_str_bad() {
        let input = PuzzleInput::try_from("");
        assert!(input.is_err());
    }
}
